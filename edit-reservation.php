<?php
require_once('header.php');
require_once('left-sidebar.php');
include 'admin/inc/autoload.php';
?>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!--bootstrap -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <link href="assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" media="screen">
    <!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="assets/plugins/material/material.min.css">
	<link rel="stylesheet" href="assets/css/material_style.css">
	<!-- animation -->
	<link href="assets/css/pages/animate_page.css" rel="stylesheet">
	<!-- Template Styles -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/theme-color.css" rel="stylesheet" type="text/css" />
	<!-- dropzone -->
    <link href="assets/plugins/dropzone/dropzone.css" rel="stylesheet" media="screen">
    <!--tagsinput-->
    <link href="assets/plugins/jquery-tags-input/jquery-tags-input.css" rel="stylesheet">
    <!--select2-->
    <link href="assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- Date Time item CSS -->
    <link rel="stylesheet" href="assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
	<!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" />

</head>
<body>
<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Reseveration Details</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Reseveration</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Reseveration</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
							<div class="col-sm-12">
								<div class="card-box">
									<div class="card-head">
										<header>Reseveration</header>
										<button id = "panel-button" 
				                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
				                           data-upgraded = ",MaterialButton">
				                           <i class = "material-icons">more_vert</i>
				                        </button>
				                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
				                           data-mdl-for = "panel-button">
				                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
				                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
				                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
				                        </ul>
									</div>
                                <?php
                                $conn = new mysqli("localhost", "root", "", "travelers_hotel");
                                $sql = $conn->query("select * from travelers_reservation where reference_no='$_GET[rno]'");
                                while ($data = $sql->fetch_array()) {
                                    
                      

                                    ?>
									<div class="card-body row">
										<div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "text" id = "reference" name="reference" value="<?php echo ('#'.$data['reference_no']);?>" readonly>
						                     <label class = "mdl-textfield__label" >Reference No</label>
											 
                                          </div>
                                          
                             
							            </div>
										<div class="col-lg-6 p-t-20"> 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="barnch" name="branch" value="<?php echo ($data['branch']); ?>" readonly tabIndex="-1">
												<label for="barnch" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="barnch" class="mdl-textfield__label">Branch</label>
												<ul data-mdl-for="barnch" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="DE">Travelers Spring</li>
													<li class="mdl-menu__item" data-val="BY">Travelers Mountain</li>
													<li class="mdl-menu__item" data-val="DE">Travelers Laqoon</li>
												</ul>
											</div>
							            </div>
										<div class="col-lg-6 p-t-20"> 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="room-type" name="room-type" value="<?php echo ($data['room_type']); ?>" readonly tabIndex="-1">
												<label for="room-type" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="room-type" class="mdl-textfield__label">Room Type</label>
												<ul data-mdl-for="room-type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="1">Double Deluxe Room</li>
													<li class="mdl-menu__item" data-val="2">Single Deluxe Room</li>
													<li class="mdl-menu__item" data-val="1">Honeymoon Suit</li>
													<li class="mdl-menu__item" data-val="2">Economy Double</li>
												</ul>
											</div>
										</div>
										<div class="col-lg-6 p-t-20"> 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="room-number" name="room-number" value="<?php echo ($data['room_no']); ?>" readonly tabIndex="-1">
												<label for="room-number" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="room-number" class="mdl-textfield__label">Room No</label>
												<ul data-mdl-for="room-number" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="1">R1</li>
													<li class="mdl-menu__item" data-val="2">R2</li>
													<li class="mdl-menu__item" data-val="3">R3</li>
													<li class="mdl-menu__item" data-val="4">R4</li>
												</ul>
											</div>
										</div>
										<!--<div class="col-lg-6 p-t-20"> 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="reservation-status" name="reservation-status"  readonly tabIndex="-1">
												<label for="reservation-status" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="reservation-status" class="mdl-textfield__label">Reseveration Status</label>
												<ul data-mdl-for="reservation-status" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="1">Booked</li>
													<li class="mdl-menu__item" data-val="2">Vacant</li>
													<li class="mdl-menu__item" data-val="2">Cleaning Process</li>
												</ul>
											</div>
										</div>-->
										<div class="col-lg-6 p-t-20"> 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<div class="input-group date form_datetime mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" >
													<input type="text" class="mdl-textfield__input" id="check-in" name="check-in" placeholder="" value="<?php echo ($data['check_in']); ?>">
													<span class="input-group-addon" style=" position: absolute;left: 92%; top: 20%;"><span class="fa fa-calendar"></span></span>	
													<label for="check-in" class = "mdl-textfield__label">Check-in</label>
												</div>
											</div>
										</div>
										
										<div class="col-lg-6 p-t-20"> 
											<div class="input-group date form_datetime mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" >
												<input type="text" class="mdl-textfield__input " id="check-out" name="check-out" placeholder="" value="<?php echo ($data['check_out']); ?>">
												
												<span class="input-group-addon" style="  position: absolute;left: 92%; top: 20%;"><span class="fa fa-calendar"></span></span>	
												
												<label for="check-out" class = "mdl-textfield__label ">Check-out</label>
						                 	</div>
							            </div>
										<div class="col-lg-6 p-t-20"> 
											<div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
												<input class = "mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="adult" name = "adult" value="<?php echo ($data['no_of_adult']); ?>">
												<label class = "mdl-textfield__label" for="adult">No Of Adult</label>
												<span class = "mdl-textfield__error">Number required!</span>
											</div>
							            </div>
										<div class="col-lg-6 p-t-20"> 
											<div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
												<input class = "mdl-textfield__input" type="text" pattern = "-?[0-9]*(\.[0-9]+)?" id="children" name = "children" value="<?php echo ($data['no_of_children']); ?>">
												<label class = "mdl-textfield__label" for = "children">No Of Children</label>
												<span class = "mdl-textfield__error">Number required!</span>
											</div>
							            </div>
										
									    <!--<div class="col-lg-6 p-t-20"> 
												
											<div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
												<input class = "mdl-textfield__input" type = "text" id = "date" placeholder="">
												
												<label for="datetime" class = "floating-label mdl-textfield__label" >Reservation Date</label>
											
										    </div>
										 </div>-->
                                             <?php

                                        }
                                        ?>                            
								         <div class="col-lg-12 p-t-20 text-center"> 
							              	<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" id="submit" name="submit">Submit</button>
											<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" id="cancel" name="cancel">Cancel</button>
							            </div>
									</div>
								</div>
								
							</div>
						</div> 
                </div>
    
	
<?php require_once('footer.php');?>
<!-- start js include path -->





</body>
<script>

$(document).ready(function(e) {
	$('#submit').click(function(){
		
		var referenceNo = document.getElementById("reference").value;
		var branch = document.getElementById("barnch").value;
		var roomType = document.getElementById("room-type").value;
		var roomNo = document.getElementById("room-number").value;
		var reservationStatus = "Booked";
		var checkin = document.getElementById("check-in").value;
		var checkout = document.getElementById("check-out").value;
		var adult = $('#adult').val();
		var children = document.getElementById("children").value;
		$.post('load/update-reservation.php',{referenceNo:referenceNo,branch:branch,roomType:roomType,roomNo:roomNo,reservationStatus:reservationStatus,checkin:checkin,checkout:checkout,adult:adult,children:children},function(res) {
							
							console.log(res);
                            console.log(adult);
		});
        window.alert("updated Successfully!");
        window.location.assign('all-reservation.php');
		
		
	})

		

})
</script>
</html>
<!-- end js include path -->