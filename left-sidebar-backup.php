 <?php 
 //session_start();
 include_once('header.php');
 $user = $_SESSION['user'];
 ?>
 <html>
 <head>
<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!--bootstrap -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="assets/plugins/material/material.min.css">
	<link rel="stylesheet" href="assets/css/material_style.css">
	<!-- animation -->
	<link href="assets/css/pages/animate_page.css" rel="stylesheet">
	<!-- Template Styles -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/theme-color.css" rel="stylesheet" type="text/css" />
	<link href=
	<!-- dropzone -->
    <link href="assets/plugins/dropzone/dropzone.css" rel="stylesheet" media="screen">
    <!-- Date Time item CSS -->
    <link rel="stylesheet" href="assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
	<!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" /> 
	<link rel="stylesheet" media="screen" type="text/css" href="css/colorpicker.css" />
<script type="text/javascript" src="js/colorpicker.js"></script>
	<script src="assets/plugins/jquery/jquery.min.js" ></script>
<script src="assets/plugins/popper/popper.min.js" ></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- bootstrap -->
<script src="assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
<!-- Common js-->
<script src="assets/js/app.js" ></script>

<script src="assets/js/theme-color.js" ></script>
<!-- Material -->
<script src="assets/plugins/material/material.min.js"></script>
<script src="assets/js/pages/material_select/getmdl-select.js" ></script>
<script src="assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
<script src="assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="assets/plugins/material-datetimepicker/datetimepicker.js"></script>
<!-- dropzone -->
<script src="assets/plugins/dropzone/dropzone.js" ></script>
<script src="assets/plugins/dropzone/dropzone-call.js" ></script>
<!-- animation -->
<script src="assets/js/pages/ui/animations.js" ></script>
</head>

 <body>
 <div class="page-container">
 			<!-- start sidebar menu -->
 			<div class="sidebar-container">
 				<div class="sidemenu-container navbar-collapse collapse fixed-menu">
	                <div id="remove-scroll">
	                    <ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
	                        <li class="sidebar-toggler-wrapper hide">
	                            <div class="sidebar-toggler">
	                                <span></span>
	                            </div>
	                        </li>
	                        <li class="sidebar-user-panel">
	                            <div class="user-panel">
	                                <div class="row">
                                            <div class="sidebar-userpic">
                                                <img src="assets/img/dp.jpg" class="img-responsive" alt=""> </div>
                                        </div>
                                        <div class="profile-usertitle">
                                            <div class="sidebar-userpic-name"> </div>
                                            <div class="profile-usertitle-job"><?php echo $user; ?> </div>
                                        </div>
                                        <div class="sidebar-userpic-btn">
									        <a class="tooltips" href="user_profile.html" data-placement="top" data-original-title="Profile">
									        	<i class="material-icons">person_outline</i>
									        </a>
									        <a class="tooltips" href="email_inbox.html" data-placement="top" data-original-title="Mail">
									        	<i class="material-icons">mail_outline</i>
									        </a>
									        <a class="tooltips" href="chat.html" data-placement="top" data-original-title="Chat">
									        	<i class="material-icons">chat</i>
									        </a>
									        <a class="tooltips" href="login.html" data-placement="top" data-original-title="Logout">
									        	<i class="material-icons">input</i>
									        </a>
									    </div>
	                            </div>
	                        </li>
	                        <li class="menu-heading">
			                	<span>Dashboard</span>
							</li>
					<?php
						if($user == 'Admin'){
					?>
	                        <li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                                <i class="material-icons">group</i>
	                                <span class="title">Staff</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="addform.php" class="nav-link ">
	                                        <span class="title">Add Staff Details</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all_staffs.html" class="nav-link ">
	                                        <span class="title">View All Staffs</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="edit_staff.html" class="nav-link ">
	                                        <span class="title">Edit Staff Details</span>
	                                    </a>
	                                </li>
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                                <i class="material-icons">vpn_key</i>
	                                <span class="title">Room Management</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="addroom.php" class="nav-link ">
	                                        <span class="title">Add Room Details</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all_rooms.html" class="nav-link ">
	                                        <span class="title">View All Rooms</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="edit_room.html" class="nav-link ">
	                                        <span class="title">Edit Room Details</span>
	                                    </a>
	                                </li>
	                            </ul>
							</li>
						<?php
							}
							else if ($user=='Guest'){
								
						?>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                                <i class="material-icons">business_center</i>
	                                <span class="title">Booking</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="roombook.php" class="nav-link ">
	                                        <span class="title">New Booking</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="view_booking.html" class="nav-link ">
	                                        <span class="title">View Booking</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="edit_booking.html" class="nav-link ">
	                                        <span class="title">Edit Booking</span>
	                                    </a>
									</li>
									<li class="nav-item">
	                                    <a href="room-status.php" class="nav-link ">
	                                        <span class="title">Room Status</span>
	                                    </a>
	                                </li>
	                            </ul>
							</li>
							<?php } ?>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                                <i class="material-icons">card_membership</i>
	                                <span class="title">Payment</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="payment.php" class="nav-link ">
	                                        <span class="title">Payment</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all_rooms.html" class="nav-link ">
	                                        <span class="title">View All Rooms</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="edit_room.html" class="nav-link ">
	                                        <span class="title">Edit Room Details</span>
	                                    </a>
	                                </li>
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                                <i class="material-icons">assignment</i>
	                                <span class="title">Report</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="add_room.html" class="nav-link ">
	                                        <span class="title">Add Room Details</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all_rooms.html" class="nav-link ">
	                                        <span class="title">View All Rooms</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="edit_room.html" class="nav-link ">
	                                        <span class="title">Edit Room Details</span>
	                                    </a>
	                                </li>
	                            </ul>
	                        </li>
	                        
			                	
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="buttons.html" class="nav-link ">
	                                        <span class="title">Buttons</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="alert.html" class="nav-link ">
	                                        <span class="title">Alert</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="tabs_accordions_navs.html" class="nav-link ">
	                                        <span class="title">Tabs &amp; Accordions</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="typography.html" class="nav-link ">
	                                        <span class="title">Typography</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="notification.html" class="nav-link ">
	                                        <span class="title">Notifications</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="sweet_alert.html" class="nav-link ">
	                                        <span class="title">Sweet Alert</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="panels.html" class="nav-link ">
	                                        <span class="title">Panels</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="grid.html" class="nav-link ">
	                                        <span class="title">Grids</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="tree.html" class="nav-link ">
	                                        <span class="title">Tree View</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="carousel.html" class="nav-link ">
	                                        <span class="title">Carousel</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="animation.html" class="nav-link ">
	                                        <span class="title">Animations</span>
	                                    </a>
	                                </li>
	                            </ul>
	                        </li>
	                        
	                        
	                                
	                           
	                    </ul>
	                </div>
                </div>
            </div>
            <!-- end sidebar menu --> 


</body>

<html>