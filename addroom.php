<?php
    require_once('header.php');
    require_once('left-sidebar.php');
	include 'admin/inc/autoload.php';

?>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
</head>
<body>

<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Add Room Details</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Rooms</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Add Room Details</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
						<div class="col-sm-12">
							<div class="card-box">
								<div class="card-head">
									<header>Add Room Details</header>
									<button id = "panel-button" 
			                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
			                           data-upgraded = ",MaterialButton">
			                           <i class = "material-icons">more_vert</i>
			                        </button>
			                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
			                           data-mdl-for = "panel-button">
			                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
			                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
			                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
			                        </ul>
								</div>
								<div class="card-body row">
						            <div class="col-lg-6 p-t-20"> 
						              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" id = "roomno">
					                     <label class = "mdl-textfield__label">Room Number</label>
					                  </div>
						            </div>
						            <div class="col-lg-6 p-t-20"> 
						              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								            <input class="mdl-textfield__input" type="text" id="room-type"  readonly tabIndex="-1">
								            <label for="room-type" class="pull-right margin-0">
								                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
								            </label>
								            <label for="room-type" class="mdl-textfield__label">Room Type</label>
								            <ul data-mdl-for="room-type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
								                <li class="mdl-menu__item" data-val="1">Double Deluxe Room</li>
								                <li class="mdl-menu__item" data-val="2">Single Deluxe Room</li>
								                <li class="mdl-menu__item" data-val="3">Honeymoon Suit </li>
								                <li class="mdl-menu__item" data-val="4">Economy Double</li>
								            </ul>
								        </div>
						           	</div>
						           	<div class="col-lg-6 p-t-20"> 
						              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								            <input class="mdl-textfield__input" type="text" id="ac"  readonly tabIndex="-1">
								            <label for="ac" class="pull-right margin-0">
								                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
								            </label>
								            <label for="ac" class="mdl-textfield__label">AC/Non AC</label>
								            <ul data-mdl-for="ac" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
								                <li class="mdl-menu__item" data-val="DE">AC</li>
								                <li class="mdl-menu__item" data-val="BY">Non AC</li>
								            </ul>
								        </div>
						            </div>
						            <div class="col-lg-6 p-t-20"> 
						              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								            <input class="mdl-textfield__input" type="text" id="branch" readonly tabIndex="-1">
								            <label for="branch" class="pull-right margin-0">
								                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
								            </label>
								            <label for="branch" class="mdl-textfield__label">Branch</label>
								            <ul data-mdl-for="branch" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
								                <li class="mdl-menu__item" data-val="1">Travelers Mountain</li>
								                <li class="mdl-menu__item" data-val="2">Travelers Spring </li>
								                <li class="mdl-menu__item" data-val="3">Travelers Laqoon</li>
								            </ul>
								        </div>
						            </div>
						            <div class="col-lg-6 p-t-20"> 
						              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								            <input class="mdl-textfield__input" type="text" id="charge" value="" readonly tabIndex="-1">
								            <label class="pull-right margin-0">
								                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
								            </label>
								            <label class="mdl-textfield__label" for="charge">Cancellation Charges</label>
								            <ul data-mdl-for="charge" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
								                <li class="mdl-menu__item" data-val="1">Free Cancellation</li>
								                <li class="mdl-menu__item" data-val="2">10% Before 24 Hours</li>
								                <li class="mdl-menu__item" data-val="1">No Cancellation Allow</li>
								            </ul>
								        </div>
						            </div>
						           	<div class="col-lg-6 p-t-20"> 
						              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								            <input class="mdl-textfield__input" type="text" id="bed" value="" readonly tabIndex="-1">
								            <label for="bed" class="pull-right margin-0">
								                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
								            </label>
								            <label for="bed" class="mdl-textfield__label">Bed Capacity</label>
								            <ul data-mdl-for="bed" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
								                <li class="mdl-menu__item" data-val="1">1</li>
								                <li class="mdl-menu__item" data-val="2">2</li>
								                <li class="mdl-menu__item" data-val="3">3</li>
								                <li class="mdl-menu__item" data-val="4">4</li>
								                <li class="mdl-menu__item" data-val="5">5</li>
								                <li class="mdl-menu__item" data-val="6">6</li>
								                <li class="mdl-menu__item" data-val="7">7</li>
								                <li class="mdl-menu__item" data-val="8">8</li>
								            </ul>
								        </div>
						           	</div>
						           	<div class="col-lg-6 p-t-20">
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "phone" maxlength="10" minlength="10">
					                     <label class = "mdl-textfield__label" for = "phone">Telephone Number</label>
					                     <span class = "mdl-textfield__error">Vaild phone number required!</span>
					                  </div>
						            </div>
						            <div class="col-lg-6 p-t-20">
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "rent">
					                     <label class = "mdl-textfield__label" for = "rent">Rent Per Night</label>
					                     <span class = "mdl-textfield__error">Price required!</span>
					                  </div>
									</div>
									
						          
                                       <div class="col-lg-6 p-t-20"> 
						              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <textarea class = "mdl-textfield__input" rows =  "2" 
					                        id = "description" ></textarea>
					                     <label class = "mdl-textfield__label" for = "text7">Room Description</label>
					                  </div>
							         </div>
									 <div class="col-lg-6 p-t-20"> 
						              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									  <form id="upload_multiple_image" class="" enctype="multipart/form-data" action="save-image.php" method="post">
					                     <input type="file" class = "mdl-textfield__input" id="files" name="files[]"  multiple accept=".jpg, .png, .gif, .jpeg" placeholder="" >
					                     <label class = "mdl-textfield__label" for = "text7">Room Picture</label>
					                  </div>
							         </div>
									 <div class="col-lg-6 p-t-20">
									
						           
									
									<!-- <input class = "mdl-textfield__input" type = "file"  id="files" name="files[]"  multiple accept=".jpg, .png, .gif, .jpeg"> -->
									   
									  </div>
							         <div class="col-lg-12 p-t-20 text-center"> 
						              	<button type="submit" id="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
										<button type="button" id="cancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
						            </div>
									</form>
								</div>
							</div>
						</div>
					</div> 
                </div>
            </div>
		
            <?php require_once('footer.php');?>
<!-- start js include path -->

</body>

<script>

$(document).ready(function(e) {
	
	$('#submit').click(function(){
		
			var image_name = $('#files').val();
			if(image_name == '')
			{
			window.alert("Please Select Image");
			return false;
			}
			window.alert("Room added Successfully!");
			var roomno = document.getElementById("roomno").value;
			var roomtype = document.getElementById("room-type").value;
			var ac = document.getElementById("ac").value;
			var branch = document.getElementById("branch").value;
			var charge = document.getElementById("charge").value;
			var bed = document.getElementById("bed").value;
			var phone = document.getElementById("phone").value;
			var rent = $('#rent').val();
			var description = document.getElementById("description").value;
			//var address = document.getElementById("address").value;
			
			$.post('load/saveroom.php',{roomno:roomno,roomtype:roomtype,ac:ac,branch:branch,charge:charge,bed:bed,phone:phone,rent:rent,description:description},function(res) {	
				console.log(res);
				$('#alert').html(res);
				
			});
			document.getElementById("roomno").value = "";
			document.getElementById("room-type").value = "";
			document.getElementById("ac").value = "";
			document.getElementById("branch").value = "";
			document.getElementById("charge").value = "";
			document.getElementById("bed").value = "";
			document.getElementById("phone").value = "";
			document.getElementById("rent").value = "";
			document.getElementById("description").value = "";	
			document.getElementById("image").value = "";

    })	
			$('#upload_multiple_image').on('submit',function(event){
		/*console.log(new FormData(this));
		  event.preventDefault();

        var image_name = $('#image').val();
        if(image_name == '')
        {
           window.alert("Please Select Image");
            return false;
        }
        else
        {
            $.ajax({
                url:"save-image.php",
                method:"POST",
                data: new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success:function(data)
                {
                    $('#image').val('');
                   // load_images();
                }
            });
        }
		
$('#upload_multiple_image').ajaxForm({
target:'#uploaded_images_preview',
beforeSubmit:function(e){
$('.file_uploading').show();
},
success:function(e){
$('.file_uploading').hide();
},
error:function(e){
}
}).submit();*/

    })		
			
	})


	
			

</script>
</html>
