<?php
require_once('header.php');
require_once('left-sidebar.php');
include 'admin/inc/autoload.php';

?>
<html>
<head>
</head>
<body>

<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Do Payment</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Payment</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Do Payment</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
						<div class="col-sm-12">
							<div class="card-box">
								<div class="card-head">
									<header>Do Payment Details</header>
									<button id = "panel-button" 
			                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
			                           data-upgraded = ",MaterialButton">
			                           <i class = "material-icons">more_vert</i>
			                        </button>
			                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
			                           data-mdl-for = "panel-button">
			                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
			                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
			                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
			                        </ul>
								</div>
								<div class="card-body row">
						            <div class="col-lg-6 p-t-20"> 
						              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" id = "reference" value="<?php echo '#' . abs(crc32(uniqid())); ?>" readonly>
					                     <label class = "mdl-textfield__label">Reference No</label>
					                  </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20"> 
						              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" id = "guest" >
					                     <label class = "mdl-textfield__label">Guest Name</label>
					                  </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20"> 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="barnch" name="branch" value="" readonly tabIndex="-1">
												<label for="barnch" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="barnch" class="mdl-textfield__label">Branch</label>
												<ul data-mdl-for="barnch" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="DE">Travelers Spring</li>
													<li class="mdl-menu__item" data-val="BY">Travelers Mountain</li>
													<li class="mdl-menu__item" data-val="DE">Travelers Laqoon</li>
												</ul>
											</div>
							            </div>
						           <div class="col-lg-6 p-t-20"> 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="room-type" name="room-type"  readonly tabIndex="-1">
												<label for="room-type" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="room-type" class="mdl-textfield__label">Room Type</label>
												<ul data-mdl-for="room-type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="1">Double Deluxe Room</li>
													<li class="mdl-menu__item" data-val="2">Single Deluxe Room</li>
													<li class="mdl-menu__item" data-val="1">Honeymoon Suit</li>
													<li class="mdl-menu__item" data-val="2">Economy Double</li>
												</ul>
											</div>
										</div>
										<div class="col-lg-6 p-t-20"> 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="room-number" name="room-number" readonly tabIndex="-1">
												<label for="room-number" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="room-number" class="mdl-textfield__label">Room No</label>
												<ul data-mdl-for="room-number" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="1">R1</li>
													<li class="mdl-menu__item" data-val="2">R2</li>
													<li class="mdl-menu__item" data-val="3">R3</li>
													<li class="mdl-menu__item" data-val="4">R4</li>
												</ul>
											</div>
										</div>
						            
						           	 <div class="col-lg-6 p-t-20">
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "days" name="days">
					                     <label class = "mdl-textfield__label" for = "days">Days</label>
					                     <span class = "mdl-textfield__error">Day required!</span>
					                  </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20" > 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="payment-type" name="payment-type" readonly tabIndex="-1">
												<label for="payment-type" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="payment-type" class="mdl-textfield__label">Payment Type</label>
												<ul data-mdl-for="payment-type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="1">Card</li>
													<li class="mdl-menu__item" data-val="2">Cash</li>
													
												</ul>
											</div>
										</div>
										
										 <div class="col-lg-6 p-t-20" id="card-type-div" hidden> 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
											<div id="paypal-button-container"></div>
												<!--<input class="mdl-textfield__input" type="text" id="card-type" name="card-type" readonly tabIndex="-1">
												<label for="card-type" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="card-type" class="mdl-textfield__label">Card Type</label>
												<ul data-mdl-for="card-type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="1">Visa</li>
													<li class="mdl-menu__item" data-val="2">Master</li>
													
												</ul>-->
											</div>
										</div>
										<!--	<div class="col-lg-6 p-t-20"  id="card-name-div"  hidden>
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        id = "card-name">
					                     <label class = "mdl-textfield__label" for = "card-name">Card Name</label>
					                     
					                  </div>
                                    </div>
									
									
                                            
										<div class="col-lg-6 p-t-20"  id="card-num-div" hidden >
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" name="creditcard" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "card-num" maxlength="16" minlength="16">
					                     <label class = "mdl-textfield__label" for = "card-num">Card Number</label>
					                     <span class = "mdl-textfield__error">Correct number required!</span>
					                  </div>
                                    </div>
									<div class="col-lg-6 p-t-20"  id="expire-div"  hidden >
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "expire" placeholder="mm/yy">
					                     <label class = "mdl-textfield__label" for = "expire">Expire Date</label>
					                     
					                  </div>
                                    </div>
									<div class="col-lg-6 p-t-20"  id="cvv-div"  hidden>
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "cvv" maxlength="3" minlength="3">
					                     <label class = "mdl-textfield__label" for = "cvv">CVV</label>
					                     <span class = "mdl-textfield__error">Correct number required!</span>
					                  </div>
                                    </div>-->
						            <div class="col-lg-6 p-t-20" hidden id="amount-div">
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "amount">
					                     <label class = "mdl-textfield__label" for = "amount">Amount</label>
					                     <span class = "mdl-textfield__error">Price required!</span>
					                  </div>
                                    </div>
                                     <div class="col-lg-6 p-t-20 text-center"> 
                                         	
						            </div>
									 <div class="col-lg-6 p-t-20" hidden id="cash-div">
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "cash">
					                     <label class = "mdl-textfield__label" for = "cash">Cash</label>
					                     <span class = "mdl-textfield__error">Number required!</span>
					                  </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20 text-center" >
						              <!-- <button type="submit" id="calculate" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Calculate</button>-->
						              	<button type="submit" id="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
										<button type="button" id="cancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
									</div>
                                     <div class="col-lg-6 p-t-20" hidden id="balance-div">
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "balance" readonly placeholder="">
					                     <label class = "mdl-textfield__label" for = "balance">Balance</label>
					                     <span class = "mdl-textfield__error">Number required!</span>
					                  </div>
									</div>
						           
							       

								</div>
							</div>
						</div>
					</div> 
                </div>
            </div>

            <?php require_once('footer.php'); ?>
<!-- start js include path -->


</body>
<?php
   // $total = $rent * $_GET['days'];
   // print_r('sada :'. $total);
?>
<script>

$(document).ready(function(e) {
	$('#payment-type').change(function(){
		
			var paytype = document.getElementById("payment-type").value;
			if(paytype=="Card")
			{
				$('#card-name-div').removeAttr('hidden');
				$('#card-type-div').removeAttr('hidden');
				$('#card-num-div').removeAttr('hidden');
				$('#expire-div').removeAttr('hidden');
				$('#cvv-div').removeAttr('hidden');
				 $('#amount-div').attr('hidden',"true");
            $('#cash-div').attr('hidden',"true");
            $('#balance-div').attr('hidden',"true");
			   $('#submit').attr('hidden',"true");
            $('#cancel').attr('hidden',"true");
			}
			else 
			{
				$('#card-name-div').attr("hidden","true");
				$('#card-type-div').attr("hidden","true");
				$('#card-num-div').attr("hidden","true");
				$('#expire-div').attr("hidden","true");
				$('#cvv-div').attr("hidden","true");
				$('#amount-div').removeAttr('hidden');
				$('#cash-div').removeAttr('hidden');
				$('#balance-div').removeAttr('hidden');
				$('#submit').removeAttr('hidden');
				$('#cancel').removeAttr('hidden');
				
			}
			
	})
	
	
   /* $('#payment-type').change(function(){
		
            $('#amount-div').removeAttr('hidden');
            $('#cash-div').removeAttr('hidden');
            $('#balance-div').removeAttr('hidden');
			var branch = document.getElementById("barnch").value;
			var roomtype = document.getElementById("room-type").value;
			var roomnumber = document.getElementById("room-number").value;
			var days = document.getElementById("days").value;
			
			
			$.post('<?= URL ?>/load/savepayment.php',{branch:branch,roomtype:roomtype,roomnumber:roomnumber,days:days},function(res) {	
				console.log(res);
			});
			
	})*/
	$('#cash').keyup(function(){
		
			
			var c = document.getElementById("cash").value;
			var a = document.getElementById("amount").value;
			
            var b = c-a;
            document.getElementById("balance").value = b;
			
	})
	$('#submit').click(function(){
		
			
			var reference = document.getElementById("reference").value;
			var guest = document.getElementById("guest").value;
			
			var branch = document.getElementById("barnch").value;
			var roomtype = document.getElementById("room-type").value;
			var roomnumber = document.getElementById("room-number").value;
			var days = document.getElementById("days").value;
			var amount = $('#amount').val();
			var cash = document.getElementById("cash").value;
			var card = document.getElementById("payment-type").value;
			var cardtype = document.getElementById("card-type").value;
			var cardname = document.getElementById("card-name").value;
			var expire = document.getElementById("expire").value;
			var cardnum = document.getElementById("card-num").value;
			var cvv = document.getElementById("cvv").value;
			//var address = document.getElementById("address").value;
			
			$.post('load/savepayment.php',{card:card,cardtype:cardtype,cardname:cardname,expire:expire,cardnum:cardnum,cvv:cvv,reference:reference,guest:guest,branch:branch,roomtype:roomtype,roomnumber:roomnumber,days:days,amount:amount,cash:cash},function(res) {	
				console.log(res);
			});
			window.alert("Payment Successfully!");
            
            document.getElementById("reference").value = "<?php echo '#' . abs(crc32(uniqid())); ?>";
			document.getElementById("guest").value = "";
			
			document.getElementById("barnch").value = "";
			document.getElementById("room-type").value = "";
			document.getElementById("room-number").value = "";
			document.getElementById("days").value = "";
            document.getElementById("amount").value = "";
			document.getElementById("cash").value = "";
			document.getElementById("payment-type").value = "";
			document.getElementById("card-type").value = "";
			document.getElementById("card-name").value = "";
			document.getElementById("expire").value = "";
			document.getElementById("card-num").value = "";
			document.getElementById("cvv").value = "";
			document.getElementById("balance").value = "";
	})




// Render the PayPal button
paypal.Button.render({
// Set your environment
env: 'sandbox', // sandbox | production

// Specify the style of the button
style: {
  layout: 'vertical',  // horizontal | vertical
  size:   'medium',    // medium | large | responsive
  shape:  'rect',      // pill | rect
  color:  'gold'       // gold | blue | silver | white | black
},

// Specify allowed and disallowed funding sources
//
// Options:
// - paypal.FUNDING.CARD
// - paypal.FUNDING.CREDIT
// - paypal.FUNDING.ELV
funding: {
  allowed: [
    paypal.FUNDING.CARD,
    paypal.FUNDING.CREDIT
  ],
  disallowed: []
},

// Enable Pay Now checkout flow (optional)
commit: true,

// PayPal Client IDs - replace with your own
// Create a PayPal app: https://developer.paypal.com/developer/applications/create
client: {
  sandbox: 'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R',
  production: '<insert production client id>'
},

payment: function (data, actions) {
  return actions.payment.create({
    payment: {
      transactions: [
        {
          amount: {
            total: '0.01',
            currency: 'USD'
          }
        }
      ]
    }
  });
},

onAuthorize: function (data, actions) {
  return actions.payment.execute()
    .then(function () {
      window.alert('Payment Complete!');
    });
}
}, '#paypal-button-container');

})
</script>
</html>