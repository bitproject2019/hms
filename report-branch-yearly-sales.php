<?php
include_once('header.php');
include_once('left-sidebar.php');
include 'admin/inc/autoload.php';
?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />
    <title>Travelers Hotel</title>
    <script>
    var date; 
    </script>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
	<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!--bootstrap -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="assets/plugins/material/material.min.css">
	<link rel="stylesheet" href="assets/css/material_style.css">
	<!-- data tables -->
    <link href="assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
	<!-- animation -->
	<link href="assets/css/pages/animate_page.css" rel="stylesheet">
	<!-- Template Styles -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
	<!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" /> 
	
	<!-- dropzone -->
    <link href="assets/plugins/dropzone/dropzone.css" rel="stylesheet" media="screen">
    
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">

    <div class="page-wrapper">
     
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    <span></span>
                </a>
             
            <div class="page-content-wrapper">
            
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Yearly Sales</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Yearly Sales</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Yearly Sales</li>
                            </ol>
                        </div>
                    </div>
                    
							          
                     <div class="row">
                        <div class="col-md-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header>Yearly Sales</header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <div class="row p-b-20">
                                  <div class="col-lg-3 p-t-5"> 
                                  
						                      
                                              <select class="form-control  select2" id="branch" name="branch">
                                              <option value="">Select Branch</option>
                                              <option value="Travelers Mountain">Travelers Mountain</option>
                                              <option value="Travelers Spring">Travelers Spring</option>
                                              <option value="Travelers Lagoon">Travelers Lagoon</option>
                                             
                                              </select>
                                             
						                        </div>
						                  <div class="col-lg-4 p-t-5"> 
                                          <label class = "" for="year" >Year</label>
                                             <input class = "" type = "text" id = "year" name="year" placeholder="2019" >
							            </div>
                                         <div class="col-lg-2 p-t-5 "> 
							              	<button type="submit" class="mdl-button  btn-pink" id="submit" name="submit">Generate</button>
											
							            </div>
                                      
                                        
                                    </div>
                                    
                                    <div class="table-scrollable">
                                    <table class="table table-hover table-checkable order-column full-width" id="example">
                                        <thead>
                                            <tr>
												<th>Reference No</th>
												<th class="center">Guest ID</th>
												<th class="center"> Room No </th>
												<th class="center"> Amount </th>
												<th class="center"> Branch</th>
												<th class="center"> Date</th>
												
												
												
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
            $conn = new mysqli("localhost", "root", "", "travelers_hotel");
            $sql = $conn->query("select * from travelers_payment WHERE  YEAR(date) = YEAR(CURRENT_DATE())");
            while ($data = $sql->fetch_array()) {
                echo '
                                                        <tr class="odd gradeX">
                                                            <td class="user-circle-img sorting_1">
                                                                #' . $data['reference_no'] . '
                                                            </td>
                                                            <td class="center">' . $data['guest'] . '</td>
                                                            <td class="center">' . $data['room_no'] . '</td>
															<td class="center">' . $data['amount'] . '</td>
															<td class="center">' . $data['branch'] . '</td>
                                                            <td class="center">
                                                                <a href="tel:444786876">
                                                                    ' . $data['date'] . ' 
                                                                </a>
                                                            </td>
                                                            
                                                            
                                                            
                                                       
                                                        ';
                                                        //$income = $data['amount'];
            }
            ?>
										</tbody>
                                    </table>
                                    </div>
                                    <?php
                                    //echo isset( $_POST['date']);
                                    $sql = $conn->query("select sum(amount) as 'total' from travelers_payment WHERE YEAR(date) = YEAR(CURRENT_DATE()) ");
                                    while ($data = $sql->fetch_array()) {
                                        $tot = $data['total'] . ' . 00 LKR';
                                    }
                                    ?>
                                    <div class="col-lg-12 p-t-20"> 
											<div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                            <label class = "" for = "income">Total Income</label>
												<input class = "" type="text" pattern = "-?[0-9]*(\.[0-9]+)?" id="income" name = "income" readonly value="<?php echo $tot; ?>">
												
												
											</div>
							            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
           
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
	  <?php #endregion
    include_once('footer.php');
    ?>
        <!-- end footer -->
	</div>

	
		<script type="text/javascript" class="init">
	
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
       buttons: [
        
            {
                extend: 'collection',
                text: 'Export <i class="fa fa-angle-down"></i>',
                buttons: [
                    {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> Copy',
                titleAttr: 'Copy'
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i> Excel',
                titleAttr: 'Excel',
				
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> CSV',
                titleAttr: 'CSV'
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i> PDF',
                titleAttr: 'PDF'
            },
			{
                extend:    'print',
                text:      '<i class="fa fa-print"></i> Print',
                titleAttr: 'print'
            }
                ]
            }
        ]
    } );
	//$("#example_filter").css("float","left");
	$(".dt-buttons").css("float","left");
	$(".dt-buttons").css("float","left");
  /* date =""
   $.ajax({
			method:'POST',
			url:"<?= URL ?>/load/daily-total-sales.php",
			data:{date:date},
			dataType:"text",
			success: function(data) {
				 //$('#example').html(data);
                  $('#income').val(data);
				 console.log(data);
			}
		});*/
	//$(".dt-buttons").css("background-color","deep-pink");
} );
	</script>
    <script>
    $('#submit').click(function(){
		
		
	 branch = document.getElementById("branch").value;
        year = document.getElementById("year").value;
		$.ajax({
			method:'POST',
			url:"load/branch-yearly-sales.php",
			data:{year:year,branch:branch},
			dataType:"text",
			success: function(data) {
				 $('#example').html(data);
                 // $('#income').html(data);
				 console.log(month);
                 console.log(year);
			}
		});
		
			$.ajax({
			method:'POST',
			url:"load/branch-yearly-total-sales.php",
			data:{year:year,branch:branch},
			dataType:"text",
			success: function(data) {
				 //$('#example').html(data);
                  $('#income').val(data);
				 console.log(data);
                 console.log(month);
                 console.log(year);
			}
		});
		
		
		
	})
    </script>
</body>
</html>