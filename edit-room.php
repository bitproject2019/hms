<?php
require_once('header.php');
require_once('left-sidebar.php');
include 'admin/inc/autoload.php';

?>
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Edit Room Details</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Rooms</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Edit Room Details</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
						<div class="col-sm-12">
							<div class="card-box">
								<div class="card-head">
									<header>Edit Room Details</header>
									<button id = "panel-button" 
			                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
			                           data-upgraded = ",MaterialButton">
			                           <i class = "material-icons">more_vert</i>
			                        </button>
			                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
			                           data-mdl-for = "panel-button">
			                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
			                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
			                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
			                        </ul>
                                </div>
                                 <?php
								$conn = new mysqli("localhost", "root", "", "travelers_hotel");
								$auto = $_GET['rno'];
                                $sql = $conn->query("select * from travelers_room where id='$_GET[rno]'");
                                while ($data = $sql->fetch_array()) {
									


                                    ?>
								<div class="card-body row">
						            <div class="col-lg-6 p-t-20"> 
						              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" id = "roomno" value="<?php echo ($data['room_nos']); ?>">
					                     <label class = "mdl-textfield__label">Room Number</label>
					                  </div>
						            </div>
						            <div class="col-lg-6 p-t-20"> 
						              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								            <input class="mdl-textfield__input" type="text" id="room-type"  readonly tabIndex="-1" value="<?php echo ($data['room_type']); ?>">
								            <label for="room-type" class="pull-right margin-0">
								                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
								            </label>
								            <label for="room-type" class="mdl-textfield__label">Room Type</label>
								            <ul data-mdl-for="room-type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
								                <li class="mdl-menu__item" data-val="1">Double Deluxe Room</li>
								                <li class="mdl-menu__item" data-val="2">Single Deluxe Room</li>
								                <li class="mdl-menu__item" data-val="3">Honeymoon Suit </li>
								                <li class="mdl-menu__item" data-val="4">Economy Double</li>
								            </ul>
								        </div>
						           	</div>
						           	<div class="col-lg-6 p-t-20"> 
						              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								            <input class="mdl-textfield__input" type="text" id="ac"  readonly tabIndex="-1" value="<?php echo ($data['ac']); ?>">
								            <label for="ac" class="pull-right margin-0">
								                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
								            </label>
								            <label for="ac" class="mdl-textfield__label">AC/Non AC</label>
								            <ul data-mdl-for="ac" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
								                <li class="mdl-menu__item" data-val="DE">AC</li>
								                <li class="mdl-menu__item" data-val="BY">Non AC</li>
								            </ul>
								        </div>
						            </div>
						            <div class="col-lg-6 p-t-20"> 
						              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								            <input class="mdl-textfield__input" type="text" id="branch" readonly tabIndex="-1" value="<?php echo ($data['branch']); ?>">
								            <label for="branch" class="pull-right margin-0">
								                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
								            </label>
								            <label for="branch" class="mdl-textfield__label">Branch</label>
								            <ul data-mdl-for="branch" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
								                <li class="mdl-menu__item" data-val="1">Travelers Mountain</li>
								                <li class="mdl-menu__item" data-val="2">Travelers Spring </li>
								                <li class="mdl-menu__item" data-val="3">Travelers Laqoon</li>
								            </ul>
								        </div>
						            </div>
						            <div class="col-lg-6 p-t-20"> 
						              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								            <input class="mdl-textfield__input" type="text" id="charge"  readonly tabIndex="-1" value="<?php echo ($data['cancellation_charge']); ?>">
								            <label class="pull-right margin-0">
								                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
								            </label>
								            <label class="mdl-textfield__label" for="charge">Cancellation Charges</label>
								            <ul data-mdl-for="charge" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
								                <li class="mdl-menu__item" data-val="1">Free Cancellation</li>
								                <li class="mdl-menu__item" data-val="2">10% Before 24 Hours</li>
								                <li class="mdl-menu__item" data-val="1">No Cancellation Allow</li>
								            </ul>
								        </div>
						            </div>
						           	<div class="col-lg-6 p-t-20"> 
						              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								            <input class="mdl-textfield__input" type="text" id="bed" readonly tabIndex="-1" value="<?php echo ($data['bed']); ?>">
								            <label for="bed" class="pull-right margin-0">
								                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
								            </label>
								            <label for="bed" class="mdl-textfield__label">Bed Capacity</label>
								            <ul data-mdl-for="bed" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
								                <li class="mdl-menu__item" data-val="1">1</li>
								                <li class="mdl-menu__item" data-val="2">2</li>
								                <li class="mdl-menu__item" data-val="3">3</li>
								                <li class="mdl-menu__item" data-val="4">4</li>
								                <li class="mdl-menu__item" data-val="5">5</li>
								                <li class="mdl-menu__item" data-val="6">6</li>
								                <li class="mdl-menu__item" data-val="7">7</li>
								                <li class="mdl-menu__item" data-val="8">8</li>
								            </ul>
								        </div>
						           	</div>
						           	<div class="col-lg-6 p-t-20">
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "phone" maxlength="10" minlength="10" value="<?php echo ($data['phone']); ?>">
					                     <label class = "mdl-textfield__label" for = "phone">Telephone Number</label>
					                     <span class = "mdl-textfield__error">Vaild phone number required!</span>
					                  </div>
						            </div>
						            <div class="col-lg-6 p-t-20">
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "rent" value="<?php echo ($data['rent']); ?>">
					                     <label class = "mdl-textfield__label" for = "rent">Rent Per Night</label>
					                     <span class = "mdl-textfield__error">Price required!</span>
					                  </div>
									</div>
									
						          
                                       <div class="col-lg-12 p-t-20"> 
						              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <textarea class = "mdl-textfield__input" rows =  "2" 
					                        id = "description" ><?php echo ($data['description']); ?></textarea>
					                     <label class = "mdl-textfield__label" for = "text7">Room Description</label>
					                  </div>
                                     </div>
                                     
										
             
									  <div class="col-lg-12 p-t-20 ">
										
						            <label class="control-label col-md-3">Upload Room Photos</label>
									 <form id="" class="dropzone" enctype="multipart/form-data" action="#" method="post">
									 <div class="card card-box">
											<div class="dz-message" >
											<?php
												// Include the database configuration file
												include_once 'connection.php';
												$conn = new mysqli("localhost", "root", "", "travelers_hotel");
												// Get images from the database
												$query = $conn->query("SELECT * FROM travelers_new_images where id='$auto'");

												if($query->num_rows > 0){
													while($row = $query->fetch_assoc()){
														$imageURL = 'uploads/'.$row["file_name"];
												?>
													<img src="<?php echo $imageURL; ?>" alt="" height="100" width="100" />
												<?php }
												}else{ ?>
													<p>No image(s) found...</p>
												<?php } ?> 
																							
										</form>
                                       </div> 
									  <div class="col-lg-12 p-t-20 ">
										
						            <label class="control-label col-md-12">To add more photos choose</label>
									 <form id="update-image-form" class="dropzone" enctype="multipart/form-data" method="post" action="">
									 <div class="card card-box">
											<div class="dz-message" >
											
																							
										
												<input class = "mdl-textfield__input" type = "file"  id="files" name="files[]"  multiple accept=".jpg, .png, .gif, .jpeg">
												<input type="text" hidden value="<?php echo $auto ;?>">
											</div>
										</div>
										
                                       </div> 
							         <div class="col-lg-12 p-t-20 text-center"> 
						              	<button type="submit" id="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
										<button type="button" id="cancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
						            </div>
									   <?php

									}
									    ?>
									</form>
								</div>
							</div>
						</div>
					</div> 
                </div>
            </div>

            <?php require_once('footer.php'); ?>
<!-- start js include path -->

</body>
<script>

$(document).ready(function(e) {
	
	$('#submit').click(function(){
		
			var auto = '<?php echo $auto;?>';
			console.log(auto);
			var roomno = document.getElementById("roomno").value;
			var roomtype = document.getElementById("room-type").value;
			var ac = document.getElementById("ac").value;
			var branch = document.getElementById("branch").value;
			var charge = document.getElementById("charge").value;
			var bed = document.getElementById("bed").value;
			var phone = document.getElementById("phone").value;
			var rent = $('#rent').val();
			var description = document.getElementById("description").value;
			//var address = document.getElementById("address").value;
			
			$.post('load/update-room.php',{auto:auto,roomno:roomno,roomtype:roomtype,ac:ac,branch:branch,charge:charge,bed:bed,phone:phone,rent:rent,description:description},function(res) {	
				console.log(res);
		
		 
        var image_name = $('#image').val();
       /* if(image_name != '')
        {
            $.ajax({
                url:"update-image.php",
                method:"POST",
                data: new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success:function(data)
                {
                    $('#image').val('');
                   // load_images();
                }
            });
		}*/
			});
            window.alert("updated Successfully!");
            window.location.assign('all-room.php');
			
	

})
/*$('#update-image-form').submit(function(){
	var auto = '<?php echo $auto;?>';
	 $.ajax({
                url:"update-img.php",
                method:"POST",
                data: new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success:function(data)
                {
                    $('#files').val('');
                   // load_images();
                }
            });
})*/
})
</script>
</html>
<?php

if(isset($_POST['submit'])){
	include('connection.php');
$conn = new mysqli("localhost", "root", "", "travelers_hotel");
/*$sql = $conn->query("SELECT auto_id FROM travelers_room ORDER BY auto_id DESC LIMIT 1");
while ($data = $sql->fetch_array()) {
    $id = $data['auto_id'];
}
if (count($_FILES["image"]["tmp_name"]) > 0) {
    for ($count = 0; $count < count($_FILES["image"]["tmp_name"]); $count++) {
        $image_file = addslashes(file_get_contents($_FILES["image"]["tmp_name"][$count]));
        $query = "INSERT INTO travelers_images(auto_id,images) VALUES ('$id','$image_file')";
        $statement = $connect->prepare($query);
        $statement->execute();
    }
}*/

$id = $auto;
    // Include the database configuration file
  //  include_once 'dbConfig.php';
    
    // File upload configuration
    $targetDir = "uploads/";
    $allowTypes = array('jpg','png','jpeg','gif');
    
    $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = '';
    if(!empty(array_filter($_FILES['files']['name']))){
        foreach($_FILES['files']['name'] as $key=>$val){
            // File upload path
            $fileName = basename($_FILES['files']['name'][$key]);
            $targetFilePath = $targetDir . $fileName;
            
            // Check whether file type is valid
            $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
            if(in_array($fileType, $allowTypes)){
                // Upload file to server
                if(move_uploaded_file($_FILES["files"]["tmp_name"][$key], $targetFilePath)){
                    // Image db insert sql
                    $insertValuesSQL .= "('".$fileName."', NOW(),'$id'),";
                }else{
                    $errorUpload .= $_FILES['files']['name'][$key].', ';
                }
            }else{
                $errorUploadType .= $_FILES['files']['name'][$key].', ';
            }
        }
        
        if(!empty($insertValuesSQL)){
            $insertValuesSQL = trim($insertValuesSQL,',');
            // Insert image file name into database
            $insert = $conn->query("INSERT INTO travelers_new_images (file_name, uploaded_on,id) VALUES $insertValuesSQL");
            if($insert){
                $errorUpload = !empty($errorUpload)?'Upload Error: '.$errorUpload:'';
                $errorUploadType = !empty($errorUploadType)?'File Type Error: '.$errorUploadType:'';
                $errorMsg = !empty($errorUpload)?'<br/>'.$errorUpload.'<br/>'.$errorUploadType:'<br/>'.$errorUploadType;
                $statusMsg = "Files are uploaded successfully.".$errorMsg;
                header('location:all-room.php');
            }else{
                 header('location:all-room.php');
               
            }
        }
	}

    
    // Display status message
	echo $statusMsg;
	//header('location:all-room.php');
}

?>

