<?php 
 //session_start();
include_once('header.php');
$user = $_SESSION['user'];
$name = $_SESSION['name'];

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />
    <title>Spice Hotel | Bootstrap 4 Admin Dashboard Template + UI Kit</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!--bootstrap -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <link href="assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" media="screen">
    <!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="assets/plugins/material/material.min.css">
	<link rel="stylesheet" href="assets/css/material_style.css">
	<!-- animation -->
	<link href="assets/css/pages/animate_page.css" rel="stylesheet">
	<!-- Template Styles -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/theme-color.css" rel="stylesheet" type="text/css" />
	<!-- dropzone -->
    <link href="assets/plugins/dropzone/dropzone.css" rel="stylesheet" media="screen">
    <!--tagsinput-->
    <link href="assets/plugins/jquery-tags-input/jquery-tags-input.css" rel="stylesheet">
    <!--select2-->
    <link href="assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" />
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
    <div class="page-wrapper">
       <!-- start header -->
		
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<div class="sidebar-container">
 				<div class="sidemenu-container navbar-collapse collapse fixed-menu">
	                <div id="remove-scroll">
	                    <ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
	                        <li class="sidebar-toggler-wrapper hide">
	                            <div class="sidebar-toggler">
	                                <span></span>
	                            </div>
	                        </li>
	                        <li class="sidebar-user-panel">
	                            <div class="user-panel">
	                                <div class="row">
                                            <div class="sidebar-userpic">
                                                <img src="assets/img/dp.jpg" class="img-responsive" alt=""> </div>
                                        </div>
                                        <div class="profile-usertitle">
                                            <div class="sidebar-userpic-name"> <?php echo $name; ?>  </div>
                                            <div class="profile-usertitle-job"> <?php echo $user; ?> </div>
                                        </div>
                                      
	                            </div>
	                        </li>
	                       
	                        <li class="nav-item start">
	                            <a href="dashboard.php" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Dashboard</span>
                                	
	                            </a>
	                            
	                        </li>
							<?php
							if($user=="Admin")
							{
							?>
							 <li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                               <i class="material-icons f-left">perm_identity</i>
	                                <span class="title">Members</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="addform.php" class="nav-link ">
	                                        <span class="title">New Member</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-persons.php" class="nav-link ">
	                                        <span class="title">View Member</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="change-password.php" class="nav-link ">
	                                        <span class="title">Change Password</span>
	                                    </a>
	                                </li>
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">location_city</i>
	                                <span class="title">Room Management</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="addroom.php" class="nav-link ">
	                                        <span class="title">New Room</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-room.php" class="nav-link ">
	                                        <span class="title">View Room</span>
	                                    </a>
									</li>
									<li class="nav-item">
	                                    <a href="room-status.php" class="nav-link ">
	                                        <span class="title">Room Status</span>
	                                    </a>
	                                </li>
	                               
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">room</i>
	                                <span class="title">Reservation</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="roombook.php" class="nav-link ">
	                                        <span class="title">New Reservation</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-reservation.php" class="nav-link ">
	                                        <span class="title">View Reservation</span>
	                                    </a>
									</li>
									<li class="nav-item">
	                                    <a href="room-status.php" class="nav-link ">
	                                        <span class="title">Room Status</span>
	                                    </a>
	                                </li>
	                                
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">credit_card</i>
	                                <span class="title">Payment</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="payment.php" class="nav-link ">
	                                        <span class="title">New Payment</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-payment.php" class="nav-link ">
	                                        <span class="title">View Payment</span>
	                                    </a>
	                                </li>
	                               
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">credit_card</i>
	                                <span class="title">Card Management</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="creditcard.php" class="nav-link ">
	                                        <span class="title">New Card</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-card.php" class="nav-link ">
	                                        <span class="title">View Card</span>
	                                    </a>
	                                </li>
	                               
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">slideshow</i>
	                                <span class="title">Reports</span>
	                                <span class="arrow "></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                         Sales
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
	                                        <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                 Travelers Spring
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                        Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                         Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                         Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
											  <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                Travelers Mount
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                        Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                       Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                         Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
											  <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                Travelers Lagoon
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                        Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                        Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                        Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-daily-sales.php" class="nav-link">
	                                                 Daily Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-monthly-sales.php" class="nav-link">
	                                                Monthly Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-yearly-sales.php" class="nav-link">
	                                                 Yearly Sales</a>
	                                        </li>
	                                    </ul>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                        Compare
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
	                                        <li class="nav-item">
	                                            <a href="report-branch-daily.php" class="nav-link">
	                                                Daily Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-branch-monthly.php" class="nav-link">
	                                                 Monthly Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-branch-yearly.php" class="nav-link">
	                                                Yearly Sales
	                                             </a>
	                                        </li>
	                                    </ul>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-payment.php" class="nav-link">
	                                        Payment </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-card.php" class="nav-link">
	                                         Card </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="report-persons.php" class="nav-link">
	                                         Members </a>
	                                </li>
									
									<li class="nav-item">
	                                    <a href="report-reservation.php" class="nav-link">
	                                         Reservation </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-room.php" class="nav-link">
	                                        Room </a>
	                                </li>
									
									<li class="nav-item">
	                                    <a href="report-room-status.php" class="nav-link">
	                                        Room Status</a>
	                                </li>
									<li class="nav-item">
	                                    <a href="room-availability.php" class="nav-link">
	                                       Room Availability</a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-feedback.php" class="nav-link">
	                                        Feedback</a>
	                                </li>
	                            </ul>
	                        </li>
							<?php	
							}
							elseif ($user=="Director") {

								# code...
							?>
							 <li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                                <i class="material-icons f-left">perm_identity</i>
	                                <span class="title">Members</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="addform.php" class="nav-link ">
	                                        <span class="title">New Member</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-persons.php" class="nav-link ">
	                                        <span class="title">View Member</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="change-password.php" class="nav-link ">
	                                        <span class="title">Change Password</span>
	                                    </a>
	                                </li>
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">location_city</i>
	                                <span class="title">Room Management</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="addroom.php" class="nav-link ">
	                                        <span class="title">New Room</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-room.php" class="nav-link ">
	                                        <span class="title">View Room</span>
	                                    </a>
									</li>
									<li class="nav-item">
	                                    <a href="room-status.php" class="nav-link ">
	                                        <span class="title">Room Status</span>
	                                    </a>
	                                </li>
	                               
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">room</i>
	                                <span class="title">Reservation</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="roombook.php" class="nav-link ">
	                                        <span class="title">New Reservation</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-reservation.php" class="nav-link ">
	                                        <span class="title">View Reservation</span>
	                                    </a>
	                                </li>
	                                
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">credit_card</i>
	                                <span class="title">Payment</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="payment.php" class="nav-link ">
	                                        <span class="title">New Payment</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-payment.php" class="nav-link ">
	                                        <span class="title">View Payment</span>
	                                    </a>
	                                </li>
	                               
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">credit_card</i>
	                                <span class="title">Card Management</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="creditcard.php" class="nav-link ">
	                                        <span class="title">New Card</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-card.php" class="nav-link ">
	                                        <span class="title">View Card</span>
	                                    </a>
	                                </li>
	                               
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">slideshow</i>
	                                <span class="title">Reports</span>
	                                <span class="arrow "></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                        Sales
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
	                                        <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                Travelers Spring
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                        Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                        Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                         Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
											  <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                 Travelers Mount
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                         Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                         Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                        Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
											  <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                Travelers Lagoon
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                        Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                         Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                         Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-daily-sales.php" class="nav-link">
	                                                Daily Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-monthly-sales.php" class="nav-link">
	                                                 Monthly Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-yearly-sales.php" class="nav-link">
	                                               Yearly Sales</a>
	                                        </li>
	                                    </ul>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                         Compare
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
	                                        <li class="nav-item">
	                                            <a href="report-branch-daily.php" class="nav-link">
	                                                Daily Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-branch-monthly.php" class="nav-link">
	                                                 Monthly Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-branch-yearly.php" class="nav-link">
	                                                Yearly Sales
	                                             </a>
	                                        </li>
	                                    </ul>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-payment.php" class="nav-link">
	                                         Payment </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-card.php" class="nav-link">
	                                         Card </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="report-persons.php" class="nav-link">
	                                         Members </a>
	                                </li>
									
									<li class="nav-item">
	                                    <a href="report-reservation.php" class="nav-link">
	                                        Reservation </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-room.php" class="nav-link">
	                                         Room </a>
	                                </li>
									
									<li class="nav-item">
	                                    <a href="report-room-status.php" class="nav-link">
	                                         Room Status</a>
	                                </li>
									<li class="nav-item">
	                                    <a href="room-availability.php" class="nav-link">
	                                         Room Availability</a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-feedback.php" class="nav-link">
	                                        Feedback</a>
	                                </li>
	                            </ul>
	                        </li>
							<?php
							}
							elseif ($user=="Sales") {
								# code...
							?>
							 <li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                                <i class="material-icons">business_center</i>
	                                <span class="title">Details</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="edit-own.php" class="nav-link ">
	                                        <span class="title">Edit Member</span>
	                                    </a>
	                                </li>
	                                
	                            </ul>
	                        </li>
							
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">room</i>
	                                <span class="title">Reservation</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="roombook.php" class="nav-link ">
	                                        <span class="title">New Reservation</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-reservation.php" class="nav-link ">
	                                        <span class="title">View Reservation</span>
	                                    </a>
	                                </li>
	                                
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">credit_card</i>
	                                <span class="title">Payment</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="payment.php" class="nav-link ">
	                                        <span class="title">New Payment</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-payment.php" class="nav-link ">
	                                        <span class="title">View Payment</span>
	                                    </a>
	                                </li>
	                               
	                            </ul>
	                        </li>
							
							<li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">slideshow</i>
	                                <span class="title">Reports</span>
	                                <span class="arrow "></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                         Sales
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
	                                        <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                               Travelers Spring
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                         Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                         Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                         Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
											  <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                               Travelers Mount
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                        Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                        Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                        Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
											  <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                 Travelers Lagoon
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                        Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                       Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                        Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-daily-sales.php" class="nav-link">
	                                                Daily Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-monthly-sales.php" class="nav-link">
	                                                 Monthly Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-yearly-sales.php" class="nav-link">
	                                                Yearly Sales</a>
	                                        </li>
	                                    </ul>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                        Compare
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
	                                        <li class="nav-item">
	                                            <a href="report-branch-daily.php" class="nav-link">
	                                                 Daily Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-branch-monthly.php" class="nav-link">
	                                                 Monthly Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-branch-yearly.php" class="nav-link">
	                                                Yearly Sales
	                                             </a>
	                                        </li>
	                                    </ul>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-payment.php" class="nav-link">
	                                        Payment </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-card.php" class="nav-link">
	                                        Card </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="report-persons.php" class="nav-link">
	                                        Members </a>
	                                </li>
									
									<li class="nav-item">
	                                    <a href="report-reservation.php" class="nav-link">
	                                         Reservation </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-room.php" class="nav-link">
	                                         Room </a>
	                                </li>
									
									<li class="nav-item">
	                                    <a href="report-room-status.php" class="nav-link">
	                                        Room Status</a>
	                                </li>
									<li class="nav-item">
	                                    <a href="room-availability.php" class="nav-link">
	                                         Room Availability</a>
	                                </li>
									
	                            </ul>
	                        </li>
							<?php
							}
							elseif ($user=="Manager") {
								# code...
							?>
							 <li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                               <i class="material-icons f-left">perm_identity</i>
	                                <span class="title">Members</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="addform.php" class="nav-link ">
	                                        <span class="title">New Member</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-persons.php" class="nav-link ">
	                                        <span class="title">View Member</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="change-password.php" class="nav-link ">
	                                        <span class="title">Change Password</span>
	                                    </a>
	                                </li>
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">location_city</i>
	                                <span class="title">Room Management</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="addroom.php" class="nav-link ">
	                                        <span class="title">New Room</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-room.php" class="nav-link ">
	                                        <span class="title">View Room</span>
	                                    </a>
									</li>
									<li class="nav-item">
	                                    <a href="room-status.php" class="nav-link ">
	                                        <span class="title">Room Status</span>
	                                    </a>
	                                </li>
	                               
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">room</i>
	                                <span class="title">Reservation</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="roombook.php" class="nav-link ">
	                                        <span class="title">New Reservation</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-reservation.php" class="nav-link ">
	                                        <span class="title">View Reservation</span>
	                                    </a>
	                                </li>
	                                
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                                <i class="material-icons">business_center</i>
	                                <span class="title">Payment</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="payment.php" class="nav-link ">
	                                        <span class="title">New Payment</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-payment.php" class="nav-link ">
	                                        <span class="title">View Payment</span>
	                                    </a>
	                                </li>
	                               
	                            </ul>
	                        </li>
							
							<li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">slideshow</i>
	                                <span class="title">Reports</span>
	                                <span class="arrow "></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                        Sales
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
	                                        <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                 Travelers Spring
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                        Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                         Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                        Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
											  <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                 Travelers Mount
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                       Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                        Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                        Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
											  <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                 Travelers Lagoon
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                         Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                        Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                        Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-daily-sales.php" class="nav-link">
	                                                 Daily Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-monthly-sales.php" class="nav-link">
	                                                Monthly Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-yearly-sales.php" class="nav-link">
	                                                Yearly Sales</a>
	                                        </li>
	                                    </ul>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                         Compare
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
	                                        <li class="nav-item">
	                                            <a href="report-branch-daily.php" class="nav-link">
	                                                Daily Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-branch-monthly.php" class="nav-link">
	                                                 Monthly Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-branch-yearly.php" class="nav-link">
	                                                Yearly Sales
	                                             </a>
	                                        </li>
	                                    </ul>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-payment.php" class="nav-link">
	                                         Payment </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-card.php" class="nav-link">
	                                        Card </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="report-persons.php" class="nav-link">
	                                         Members </a>
	                                </li>
									
									<li class="nav-item">
	                                    <a href="report-reservation.php" class="nav-link">
	                                        Reservation </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-room.php" class="nav-link">
	                                         Room </a>
	                                </li>
									
									<li class="nav-item">
	                                    <a href="report-room-status.php" class="nav-link">
	                                         Room Status</a>
	                                </li>
									<li class="nav-item">
	                                    <a href="room-availability.php" class="nav-link">
	                                         Room Availability</a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-feedback.php" class="nav-link">
	                                        Feedback</a>
	                                </li>
	                            </ul>
	                        </li>
							<?php
							}
							elseif ($user=="IT Manager") {
								# code...
							?>
							 <li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                               <i class="material-icons f-left">perm_identity</i>
	                                <span class="title">Members</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="addform.php" class="nav-link ">
	                                        <span class="title">New Member</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-persons.php" class="nav-link ">
	                                        <span class="title">View Member</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="change-password.php" class="nav-link ">
	                                        <span class="title">Change Password</span>
	                                    </a>
	                                </li>
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">location_city</i>
	                                <span class="title">Room Management</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="addroom.php" class="nav-link ">
	                                        <span class="title">New Room</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-room.php" class="nav-link ">
	                                        <span class="title">View Room</span>
	                                    </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="room-status.php" class="nav-link ">
	                                        <span class="title">Room Status</span>
	                                    </a>
	                                </li>
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">room</i>
	                                <span class="title">Reservation</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="roombook.php" class="nav-link ">
	                                        <span class="title">New Reservation</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-reservation.php" class="nav-link ">
	                                        <span class="title">View Reservation</span>
	                                    </a>
	                                </li>
	                                
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">credit_card</i>
	                                <span class="title">Payment</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="payment.php" class="nav-link ">
	                                        <span class="title">New Payment</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-payment.php" class="nav-link ">
	                                        <span class="title">View Payment</span>
	                                    </a>
	                                </li>
	                               
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">credit_card</i>
	                                <span class="title">Card Management</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="creditcard.php" class="nav-link ">
	                                        <span class="title">New Card</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-card.php" class="nav-link ">
	                                        <span class="title">View Card</span>
	                                    </a>
	                                </li>
	                               
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">slideshow</i>
	                                <span class="title">Reports</span>
	                                <span class="arrow "></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                        Sales
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
	                                        <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                Travelers Spring
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                         Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                        Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                         Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
											  <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                Travelers Mount
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                         Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                         Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                         Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
											  <li class="nav-item">
	                                            <a href="javascript:;" class="nav-link nav-toggle">
	                                                Travelers Lagoon
	                                                <span class="arrow "></span>
	                                            </a>
	                                            <ul class="sub-menu">
	                                                <li class="nav-item">
	                                                    <a href="report-branch-daily-sales.php" class="nav-link">
	                                                        Daily Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-monthly-sales.php" class="nav-link">
	                                                        Monthly Sales</a>
	                                                </li>
	                                                <li class="nav-item">
	                                                    <a href="report-branch-yearly-sales.php" class="nav-link">
	                                                       Yearly Sales</a>
	                                                </li>
	                                            </ul>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-daily-sales.php" class="nav-link">
	                                                Daily Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-monthly-sales.php" class="nav-link">
	                                               Monthly Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-yearly-sales.php" class="nav-link">
	                                               Yearly Sales</a>
	                                        </li>
	                                    </ul>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                        Compare
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
	                                        <li class="nav-item">
	                                            <a href="report-branch-daily.php" class="nav-link">
	                                                 Daily Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-branch-monthly.php" class="nav-link">
	                                                 Monthly Sales</a>
	                                        </li>
	                                        <li class="nav-item">
	                                            <a href="report-branch-yearly.php" class="nav-link">
	                                                Yearly Sales
	                                             </a>
	                                        </li>
	                                    </ul>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-payment.php" class="nav-link">
	                                       Payment </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-card.php" class="nav-link">
	                                         Card </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="report-persons.php" class="nav-link">
	                                         Members </a>
	                                </li>
									
									<li class="nav-item">
	                                    <a href="report-reservation.php" class="nav-link">
	                                         Reservation </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-room.php" class="nav-link">
	                                         Room </a>
	                                </li>
									
									<li class="nav-item">
	                                    <a href="report-room-status.php" class="nav-link">
	                                         Room Status</a>
	                                </li>
									<li class="nav-item">
	                                    <a href="room-availability.php" class="nav-link">
	                                         Room Availability</a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-feedback.php" class="nav-link">
	                                         Feedback</a>
	                                </li>
	                            </ul>
	                        </li>
							<?php
							}
							elseif ($user=="Worker") {
								# code...
							?>
							
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">location_city</i>
	                                <span class="title">Room Management</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="addroom.php" class="nav-link ">
	                                        <span class="title">New Room</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-room.php" class="nav-link ">
	                                        <span class="title">View Room</span>
	                                    </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="room-status.php" class="nav-link ">
	                                        <span class="title">Room Status</span>
	                                    </a>
	                                </li>
	                            </ul>
	                        </li>
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">room</i>
	                                <span class="title">Reservation</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="roombook.php" class="nav-link ">
	                                        <span class="title">New Reservation</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-reservation.php" class="nav-link ">
	                                        <span class="title">View Reservation</span>
	                                    </a>
	                                </li>
	                                
	                            </ul>
	                        </li>
						
							
							<li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">slideshow</i>
	                                <span class="title">Reports</span>
	                                <span class="arrow "></span>
	                            </a>
	                            <ul class="sub-menu">
	                                
	                                
									<li class="nav-item">
	                                    <a href="report-reservation.php" class="nav-link">
	                                       Reservation </a>
	                                </li>
									<li class="nav-item">
	                                    <a href="report-room.php" class="nav-link">
	                                         Room </a>
	                                </li>
									
									<li class="nav-item">
	                                    <a href="report-room-status.php" class="nav-link">
	                                       Room Status</a>
	                                </li>
									<li class="nav-item">
	                                    <a href="room-availability.php" class="nav-link">
	                                       Room Availability</a>
	                                </li>
									
	                            </ul>
	                        </li>
							<?php
							}
							elseif ($user=="Guest") {
								# code...
							?>
							 <li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                             <i class="material-icons f-left">perm_identity</i>
	                                <span class="title">Members</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="edit-own.php" class="nav-link ">
	                                        <span class="title">Details</span>
	                                    </a>
	                                </li>
	                              
	                                <li class="nav-item">
	                                    <a href="change-password.php" class="nav-link ">
	                                        <span class="title">Change Password</span>
	                                    </a>
	                                </li>
	                            </ul>
	                        </li>
						
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">room</i>
	                                <span class="title">Reservation</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="roombook.php" class="nav-link ">
	                                        <span class="title">New Reservation</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-reservation-guest.php" class="nav-link ">
	                                        <span class="title">View Reservation</span>
	                                    </a>
	                                </li>
	                                
	                            </ul>
	                        </li>
						
							<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
								<i class="material-icons f-left">credit_card</i>
	                                <span class="title">Card Management</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="creditcard.php" class="nav-link ">
	                                        <span class="title">New Card</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="all-card.php" class="nav-link ">
	                                        <span class="title">View Card</span>
	                                    </a>
	                                </li>
	                               
	                            </ul>
	                        </li>
								<li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                                <i class="material-icons">business_center</i>
	                                <span class="title">Reports</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="report-reservation.php" class="nav-link ">
	                                        <span class="title">Reservation</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item">
	                                    <a href="report-room.php" class="nav-link ">
	                                        <span class="title">Room</span>
	                                    </a>
	                                </li>
									 <li class="nav-item">
	                                    <a href="report-room-status.php" class="nav-link ">
	                                        <span class="title">Room Status</span>
	                                    </a>
	                                </li>
									 <li class="nav-item">
	                                    <a href="room-availability.php" class="nav-link ">
	                                        <span class="title">Room Availability</span>
	                                    </a>
	                                </li>
	                               <li class="nav-item">
	                                    <a href="report-feedback.php" class="nav-link ">
	                                        <span class="title">Feedback</span>
	                                    </a>
	                                </li>
	                            </ul>
	                        </li>
						
							<?php
							}
							?>
	                       
	                        
	                        
	                        
	                    </ul>
	                </div>
                </div>
            </div>
			            
    <!-- start js include path -->
    <script src="assets/plugins/jquery/jquery.min.js" ></script>
    <script src="assets/plugins/popper/popper.min.js" ></script>
    <script src="assets/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- bootstrap -->
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
    <script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" ></script>
    <script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker-init.js" ></script>
    <script src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
    <script src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
    <!-- Common js-->
	<script src="assets/js/app.js" ></script>
    <script src="assets/js/layout.js" ></script>
	<script src="assets/js/theme-color.js" ></script>
	<!-- Material -->
	<script src="assets/plugins/material/material.min.js"></script>
	<!-- animation -->
	<script src="assets/js/pages/ui/animations.js" ></script>
	<!-- dropzone -->
    <script src="assets/plugins/dropzone/dropzone.js" ></script>
    <!--tags input-->
    <script src="assets/plugins/jquery-tags-input/jquery-tags-input.js" ></script>
    <script src="assets/plugins/jquery-tags-input/jquery-tags-input-init.js" ></script>
    <!--select2-->
    <script src="assets/plugins/select2/js/select2.js" ></script>
    <script src="assets/js/pages/select2/select2-init.js" ></script>
    <!-- end js include path -->
</body>
</html>