<?php
require_once('header.php');
require_once('left-sidebar.php');
include 'admin/inc/autoload.php';
?>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/dob.js" type="text/javascript"> </script>
</head>
<body>
<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Add Feedback</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Feedback</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Add Feedback</li>
                            </ol>
                        </div>
                    </div>
    <form method='POST'>
                     <div class="row">
							<div class="col-sm-12">
								<div class="card-box">
									<div class="card-head">
										<header>Feedback Information</header>
										<button id = "panel-button" 
				                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
				                           data-upgraded = ",MaterialButton">
				                           <i class = "material-icons">more_vert</i>
				                        </button>
				                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
				                           data-mdl-for = "panel-button">
				                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
				                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
				                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
				                        </ul>
									</div>
									<div class="card-body row">
									<div class="col-lg-6 p-t-20"> 
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
											<input class="mdl-textfield__input" type="text" id="user-type" value="" readonly tabIndex="-1">
											<label for="user-type" class="pull-right margin-0">
												<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
											</label>
											<label for="user-type" class="mdl-textfield__label">Branch</label>
											<ul data-mdl-for="user-type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
												<li class="mdl-menu__item" data-val="BY">Travelers Mountain</li>
												<li class="mdl-menu__item" data-val="BY">Travelers Spring</li>
												<li class="mdl-menu__item" data-val="DE">Travelers Laqoon</li>
												
											</ul>
										</div>
							            </div>
							            <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "text" id = "firstname">
						                     <label class = "mdl-textfield__label" >First Name</label>
						                  </div>
							            </div>
                                        <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "text" id = "firstname">
						                     <label class = "mdl-textfield__label" >Subject</label>
						                  </div>
							            </div>
                                        <div class="col-lg-6 p-t-20"> 
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
                                                    <input class="mdl-textfield__input" type="text" id="ac"  readonly tabIndex="-1">
                                                    <label for="ac" class="pull-right margin-0">
                                                        <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                                    </label>
                                                    <label for="ac" class="mdl-textfield__label">Rating</label>
                                                    <ul data-mdl-for="ac" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                        <li class="mdl-menu__item" data-val="DE">Excellent</li>
                                                        <li class="mdl-menu__item" data-val="BY">Very Good</li>
                                                        <li class="mdl-menu__item" data-val="DE">Good</li>
                                                        <li class="mdl-menu__item" data-val="DE">Average</li>
                                                        <li class="mdl-menu__item" data-val="DE">Not Satisfied</li>
                                                        
                                                    </ul>
                                                </div>
                                            </div>
										</div>
                                        <div class="col-lg-12 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <textarea class = "mdl-textfield__input" rows =  "1" 
						                        id = "address" ></textarea>
						                     <label class = "mdl-textfield__label" for = "address">Message</label>
						                  </div>
								         </div>
							            
                                         <div class="col-lg-6 p-t-20"> 
						              
								         <div class="col-lg-12 p-t-20 text-center"> 
							              	<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" id="submit">Submit</button>
											<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" id="cancel">Cancel</button>
							            </div>
									</div>
								</div>
							</div>
						</div> 
                </div>
            </div>
        </form>
<?php require_once('footer.php');?>
<!-- start js include path -->


</body>
<script>

$(document).ready(function(e) {
	$( "form" ).on( "submit", function( event ) {
                    event.preventDefault();
                    var data = $( this ).serialize();
                    console.log( $( this ).serialize() );

                    // $.post('load/save-feedback.php',data,function(res){
                        
					// 	alert(res);
					// 	$('form input[type="text"],input[type="email"],input[type="password"],texatrea, select').val('');
                    //     console.log(res);
                    // })  
                });
	


})
</script>
</html>
<!-- end js include path -->