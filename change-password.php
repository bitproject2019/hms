<?php
require_once('header.php');
require_once('left-sidebar.php');
include 'admin/inc/autoload.php';
?>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/dob.js" type="text/javascript"> </script>
</head>
<body>
<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Change Password</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Change Password</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Change Password</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
							<div class="col-sm-12">
								<div class="card-box">
									<div class="card-head">
										<header>Change Your Password</header>
										<button id = "panel-button" 
				                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
				                           data-upgraded = ",MaterialButton">
				                           <i class = "material-icons">more_vert</i>
				                        </button>
				                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
				                           data-mdl-for = "panel-button">
				                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
				                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
				                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
				                        </ul>
									</div>
									<div class="card-body row">
									
							            <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "text" id = "username" name= "username">
						                     <label class = "mdl-textfield__label" >Username</label>
						                  </div>
							            </div>
                                        
							             <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "password" id = "current">
						                     <label class = "mdl-textfield__label" >Current Password</label>
						                   </div>
							            </div>
										<div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "password" id = "password" minlength="8">
											 <label class = "mdl-textfield__label" >New Password</label>
											 <span class = "mdl-textfield__error">Enter minimum 8 characters</span>
						                  </div>
							            </div>
	                                 	<div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
											 <input class = "mdl-textfield__input" type = "password" id = "confirm_password" onkeyup="checkPass(); return false;">
											 
											 <input  type = "text" id = "pass" style="color: #d50000; position: absolute; font-size: 12px; margin-top: 3px; border: none; display: block;" >
											 <label class = "mdl-textfield__label" >Confirm Password</label>
											 <span class = "mdl-textfield__error" id=""></span>
						                  </div>
							            </div>
										
                                        
								         <div class="col-lg-12 p-t-20 text-center"> 
							              	<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" id="submit">Submit</button>
											<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" id="cancel">Cancel</button>
							            </div>
									</div>
								</div>
							</div>
						</div> 
                </div>
            </div>
            <?php
                  
                                   /* $conn = new mysqli("localhost", "root", "", "travelers_hotel");
                                    $sql = $conn->query("select * from travelers_login where email='$_GET[username]'");
                                    $auto = $_GET['rno'];
                                    while ($data = $sql->fetch_array()) {

                                    }*/
            
            ?>
<?php require_once('footer.php');?>
<!-- start js include path -->

</body>
<script>

$(document).ready(function(e) {
	
	
	$('#submit').click(function(){
		 if ($('#password').val() == $('#confirm_password').val()) {
			window.alert("User added Successfully!");
			var current = document.getElementById("current").value;
			var password = document.getElementById("password").value;
			var username = document.getElementById("username").value;
			
			$.post('load/check-pass.php',{username:username,password:password,current:current},function(res) {

				console.log(res);
                if(res==$.md5(current))
                {
                    if ($('#password').val() == $('#confirm_password').val()) {
                        $.post('load/change-pass.php',{username:username,password:password},function(res) {

                         console.log(res);
                    

                        });
                    }
                }

			});
			
		 }
		 else{
			document.getElementById('pass').value = "Recheck Password";
		 }
			
			document.getElementById("username").value = "";
			
			document.getElementById("password").value = "";
			
			document.getElementById("confirm_password").value = "";
	})


})
</script>
</html>
<!-- end js include path -->