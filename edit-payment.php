<?php
require_once('header.php');
require_once('left-sidebar.php');
include 'admin/inc/autoload.php';
include 'load/savepayment.php';
?>
<html>
<head>
</head>
<body>

<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Do Payment</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Payment</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Do Payment</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
						<div class="col-sm-12">
							<div class="card-box">
								<div class="card-head">
									<header>Do Payment Details</header>
									<button id = "panel-button" 
			                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
			                           data-upgraded = ",MaterialButton">
			                           <i class = "material-icons">more_vert</i>
			                        </button>
			                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
			                           data-mdl-for = "panel-button">
			                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
			                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
			                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
			                        </ul>
                                </div>
                                  <?php
                                    $conn = new mysqli("localhost", "root", "", "travelers_hotel");
                                    $sql = $conn->query("select * from travelers_payment where reference_no='$_GET[rno]'");
                                    while ($data = $sql->fetch_array()) {



                                        ?>
								<div class="card-body row">
						            <div class="col-lg-6 p-t-20"> 
						              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" id = "reference" value="<?php echo ('#'.$data['reference_no']); ?>" readonly>
					                     <label class = "mdl-textfield__label">Reference No</label>
					                  </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20"> 
						              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" id = "guest" value="<?php echo ($data['guest']); ?>">
					                     <label class = "mdl-textfield__label">Guest Name</label>
					                  </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20"> 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="barnch" name="branch" value="" readonly tabIndex="-1" value="<?php echo ($data['branch']); ?>">
												<label for="barnch" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="barnch" class="mdl-textfield__label">Branch</label>
												<ul data-mdl-for="barnch" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="DE">Travelers Spring</li>
													<li class="mdl-menu__item" data-val="BY">Travelers Mountain</li>
													<li class="mdl-menu__item" data-val="DE">Travelers Laqoon</li>
												</ul>
											</div>
							            </div>
						           <div class="col-lg-6 p-t-20"> 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="room-type" name="room-type"  readonly tabIndex="-1" value="<?php echo ($data['room_type']); ?>">
												<label for="room-type" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="room-type" class="mdl-textfield__label">Room Type</label>
												<ul data-mdl-for="room-type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="1">Double Deluxe Room</li>
													<li class="mdl-menu__item" data-val="2">Single Deluxe Room</li>
													<li class="mdl-menu__item" data-val="1">Honeymoon Suit</li>
													<li class="mdl-menu__item" data-val="2">Economy Double</li>
												</ul>
											</div>
										</div>
										<div class="col-lg-6 p-t-20"> 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="room-number" name="room-number" readonly tabIndex="-1" value="<?php echo ($data['room_no']); ?>">
												<label for="room-number" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="room-number" class="mdl-textfield__label">Room No</label>
												<ul data-mdl-for="room-number" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="1">R1</li>
													<li class="mdl-menu__item" data-val="2">R2</li>
													<li class="mdl-menu__item" data-val="3">R3</li>
													<li class="mdl-menu__item" data-val="4">R4</li>
												</ul>
											</div>
										</div>
						            
						           	 <div class="col-lg-6 p-t-20">
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "days" name="days" value="<?php echo ($data['days']); ?>">
					                     <label class = "mdl-textfield__label" for = "days">Days</label>
					                     <span class = "mdl-textfield__error">Day required!</span>
					                  </div>
                                    </div>
                                    
						            <div class="col-lg-6 p-t-20"  id="amount-div">
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "amount" value="<?php echo ($data['amount']); ?>">
					                     <label class = "mdl-textfield__label" for = "amount">Amount</label>
					                     <span class = "mdl-textfield__error">Price required!</span>
					                  </div>
                                    </div>
                                     <div class="col-lg-6 p-t-20 text-center"> 
                                         	
						            </div>
									 <div class="col-lg-6 p-t-20"  id="cash-div">
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "cash" value="<?php echo ($data['cash']); ?>">
					                     <label class = "mdl-textfield__label" for = "cash">Cash</label>
					                     <span class = "mdl-textfield__error">Number required!</span>
					                  </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20 text-center" >
						               <button type="submit" id="calculate" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" hidden>Calculate</button>
						              	<button type="submit" id="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
										<button type="button" id="cancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
									</div>
                                     <div class="col-lg-6 p-t-20"  id="balance-div">
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "balance" readonly placeholder="">
					                     <label class = "mdl-textfield__label" for = "balance">Balance</label>
					                     <span class = "mdl-textfield__error">Number required!</span>
					                  </div>
									</div>
						              <?php

                }
                ?>   
							        
								</div>
							</div>
						</div>
					</div> 
                </div>
            </div>

            <?php require_once('footer.php'); ?>
<!-- start js include path -->

</body>
<?php
   // $total = $rent * $_GET['days'];
   // print_r('sada :'. $total);
?>
<script>

$(document).ready(function(e) {
    var cash = document.getElementById("cash").value;
	var amount = document.getElementById("amount").value;
    var balance = cash-amount;
    document.getElementById("balance").value = balance;
	$('#cash').keyup(function(){
		
			
			var c = document.getElementById("cash").value;
			var a = document.getElementById("amount").value;
			
            var b = c-a;
            document.getElementById("balance").value = b;
			
	})
	$('#submit').click(function(){
		
			
			var reference = document.getElementById("reference").value;
			var guest = document.getElementById("guest").value;
			
			var branch = document.getElementById("barnch").value;
			var roomtype = document.getElementById("room-type").value;
			var roomnumber = document.getElementById("room-number").value;
			var days = document.getElementById("days").value;
			var amount = $('#amount').val();
			var cash = document.getElementById("cash").value;
			//var address = document.getElementById("address").value;
			
			$.post('load/update-payment.php',{reference:reference,guest:guest,branch:branch,roomtype:roomtype,roomnumber:roomnumber,days:days,amount:amount,cash:cash},function(res) {	
				console.log(res);
			});
            
             window.alert("updated Successfully!");
            window.location.assign('all-payment.php');
	})


})
</script>
</html>