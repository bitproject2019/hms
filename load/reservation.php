<?php
session_start();
if ($_SERVER['REQUEST_METHOD'] === "POST") :
    include '../admin/inc/autoload.php';

$User = new User;
$Sql = new Sql;

//$User->validUserForLogin();
//$activeUser = $User->activeUser();
//$activeUser->ID;
$requiredFields = array_fill_keys(
    array(
        'referenceNo', 
        'branch',
        'roomType',
        'roomNo',
        'reservationStatus',
        'checkin',
        'checkout',
        'adult',
        'children',
        'id' 
    ),
    null
);

extract(
    array_intersect_key(
        array_merge(
            $requiredFields,
            $_POST
        ),
        $requiredFields
    )
);

//header('Content-type: application/json');
$currentUser1 = $Sql->arrayToJson(
    $Sql->select(
        array(
            'sql' => '
						                    
                        
                        SELECT room_nos 
                        FROM ' . $Sql->tblroom . '
                        LEFT JOIN ' . $Sql->tblstatus . ' ON ' . $Sql->tblstatus . ' .room_number = ' . $Sql->tblroom . '.room_nos 
                        WHERE ' . $Sql->tblstatus . ' .room_status = "Vacant"
					',
            'limit' => true
        )
    )
);

//print_r($currentUser -> $data);
//print_r($currentUser[data]);
/*$test = array($currentUser1);
$res['testsen'] = $test;
$res['testsen'] = $test['data'];*/
foreach ($currentUser1 as $event => $key) :
    

endforeach; 
//print_r($view);

$currentUsers = $Sql->arrayToJson(
    $Sql->select(
        array(
            'qry' => true,
            'sql' => '
						SELECT *
						FROM ' . $Sql->tblstatus . '
						WHERE 1 
                            AND room_number = "' . $roomNo . '"
                            AND room_type = "' . $roomType . '"
                            AND branch_no = "' . $branch . '"
                       	LIMIT 1
					',
            'limit' => true
        )
    )
);
//$res['test']=$currentUser;
//print_r($currentUser -> $data);
//print_r($currentUser[data]);
foreach ($currentUsers as $event => $view) :

    $rent = $view;

endforeach;
//print_r($view);
if ($currentUsers->error) {
    $res[] = $inserts = $Sql->insert(
        array(
            'sql' => '
					INSERT INTO ' . $Sql->tblstatus . '
					' . $Sql->insertFields(
                array(
                    'insertFields' => array(
                        'branch_no' => '' . $branch . '',
                        'room_type' => '' . $roomType . '',
                        'room_number' => '' . $roomNo . '',
                        'room_status' => '' . $reservationStatus . '',

                    )
                )
            )
        )
    );
} else {
    $updates = array(
        'updateFields' => array(
            'room_status' => '' . $reservationStatus . '',

        )
    );
    $updateUser = $Sql->update(
        array(
            'sql' => '
							UPDATE ' . $Sql->tblstatus . '
							' . $Sql->updateFields($updates) . '
							WHERE 1								 
                                AND room_number = "' . $roomNo . '"
                                AND room_type = "' . $roomType . '"
                                AND branch_no = "' . $branch . '"
							'
        )
    );
}


  $res[] =  $insertNew = $Sql->insert(
        array(
            'sql' => '
					INSERT INTO ' . $Sql->tblreservation . '
					' . $Sql->insertFields(
                array(
                    'insertFields' => array(
                        'reference_no' => '' . substr($referenceNo, 1) . '',
                        'branch' => '' . $branch. '',
                        'room_type' => '' . $roomType . '',
                        'room_no' => '' . $roomNo . '',
                        'reservation_status' => '' . $reservationStatus . '',
                        'check_in' => '' . $checkin . '',
                        'check_out' => '' . $checkout . '',
                        'no_of_adult' => '' . $adult . '',
                        'no_of_children' => $children ,
                        'guest_id' =>'' . $id . '',
                    )
                )
            )
        )
    );


  
echo json_encode($res);
endif;
?>