<?php
if ($_SERVER['REQUEST_METHOD'] === "POST") :
    include '../admin/inc/autoload.php';

$User = new User;
$Sql = new Sql;

//$User->validUserForLogin();
//$activeUser = $User->activeUser();
//$activeUser->ID;
$requiredFields = array_fill_keys(
    array(
        'reference',
        'guest',
        'branch',
        'roomtype',
        'roomnumber',
        'days',
        'amount',
        'cash'
    ),
    null
);

extract(
    array_intersect_key(
        array_merge(
            $requiredFields,
            $_POST
        ),
        $requiredFields
    )
);

header('Content-type: application/json');

$updates = array(
    'updateFields' => array(

        'guest' => '' . $guest . '',
        'branch' => '' . $branch . '',
        'room_type' => '' . $roomtype . '',
        'room_no' => '' . $roomnumber . '',
        'days' => '' . $days . '',
        'amount' => '' . $amount . '',
        'cash' => '' . $cash . '',

    )
);
$res = $updateUser = $Sql->update(
    array(
        'sql' => '
							UPDATE ' . $Sql->tblpayment . '
							' . $Sql->updateFields($updates) . '
							WHERE 1								 
                                AND reference_no="' . substr($reference, 1) . '"
                               
							'
    )
);



echo json_encode($res);
//$res['test']= $rent;
endif;
?>
