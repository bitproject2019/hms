<?php
if ($_SERVER['REQUEST_METHOD'] === "POST") :
    include '../admin/inc/autoload.php';

$User = new User;
$Sql = new Sql;

//$User->validUserForLogin();
//$activeUser = $User->activeUser();
//$activeUser->ID;
$requiredFields = array_fill_keys(
    array(
        'referenceNo',
        'branch',
        'roomType',
        'roomNo',
        'reservationStatus',
        'checkin',
        'checkout',
        'adult',
        'children'
    ),
    null
);

extract(
    array_intersect_key(
        array_merge(
            $requiredFields,
            $_POST
        ),
        $requiredFields
    )
);

//header('Content-type: application/json');

    $updates = array(
        'updateFields' => array(
            
            
            'branch' => '' . $branch . '',
            'room_type' => '' . $roomType . '',
            'room_no' => '' . $roomNo . '',
            'reservation_status' => '' . $reservationStatus . '',
            'check_in' => '' . $checkin . '',
            'check_out' => '' . $checkout . '',
            'no_of_adult' => '' . $adult . '',
            'no_of_children' => '' .  $children . '',

        )
    );
   $res= $updateUser = $Sql->update(
        array(
            'sql' => '
							UPDATE ' . $Sql->tblreservation . '
							' . $Sql->updateFields($updates) . '
							WHERE 1								 
                                AND reference_no="'.substr($referenceNo, 1).'"
                               
							'
        )
    );



echo json_encode($updateUser);
endif;
?>