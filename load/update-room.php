<?php
if ($_SERVER['REQUEST_METHOD'] === "POST") :
    include '../admin/inc/autoload.php';

$User = new User;
$Sql = new Sql;

//$User->validUserForLogin();
//$activeUser = $User->activeUser();
//$activeUser->ID;
$requiredFields = array_fill_keys(
    array(
        'roomno',
        'roomtype',
        'ac',
        'branch',
        'charge',
        'bed',
        'phone',
        'rent',
        'description',
        'auto'
    ),
    null
);

extract(
    array_intersect_key(
        array_merge(
            $requiredFields,
            $_POST
        ),
        $requiredFields
    )
);

//header('Content-type: application/json');

$updates = array(
    'updateFields' => array(


        'room_nos' => '' . $roomno . '',
        'room_type' => '' . $roomtype . '',
        'ac' => '' . $ac . '',
        'branch' => '' . $branch . '',
        'cancellation_charge' => '' . $charge . '',
        'bed' => '' . $bed . '',
        'phone' => '' . $phone . '',
        'rent' => '' . $rent . '',
        'description' => $description,

    )
);
$res = $updateUser = $Sql->update(
    array(
        'sql' => '
							UPDATE ' . $Sql->tblroom . '
							' . $Sql->updateFields($updates) . '
							WHERE 1								 
                                AND room_nos="'.$roomno.'"
                               
							'
    )
);



echo json_encode($updateUser);
endif;
?>