<?php
if ($_SERVER['REQUEST_METHOD'] === "POST") :
    include '../admin/inc/autoload.php';

$User = new User;
$Sql = new Sql;

//$User->validUserForLogin();
//$activeUser = $User->activeUser();
//$activeUser->ID;
$requiredFields = array_fill_keys(
    array(
        'userType',
        'firstname',
        'lastname',
        'nic',
        'dob',
        'gender',
        'mobile',
        'email',
        'password',
        'address',
        'children',
        'adult'
    ),
    null
);

extract(
    array_intersect_key(
        array_merge(
            $requiredFields,
            $_POST
        ),
        $requiredFields
    )
);

header('Content-type: application/json');
$currentUser = $Sql->arrayToJson(
    $Sql->select(
        array(
            'qry' => true,
            'sql' => '
						SELECT user_id
						FROM ' . $Sql->tblperson . '
						WHERE 1 
                            AND user_type = "' . $userType . '"
                        ORDER BY 
                            user_id 
                        DESC
						LIMIT 1
					',
            'limit' => true
        )
    )
);

foreach ($currentUser->data as $event => $view) :
    //print_r($view);
    $user = $view ;
    $userId = substr($user, 0, 1);
    $userIdNum = substr($user, 1);
    $userIdNum = $userIdNum + 1;


    $userId = $userId . $userIdNum;
    //print_r($userId);
endforeach;
 

if($userType !="Guest")
{
    $res[] = $insertNew = $Sql->insert(
        array(
            'sql' => '
                        INSERT INTO ' . $Sql->tbllogin . '
                        ' . $Sql->insertFields(
                array(
                    'insertFields' => array(
                        'user_id' => '' . $userId . '',
                        'email' => '' . $email . '',
                        'password' => '' . md5($password) . '',
                        'user_type' => '' . $userType . '',

                    )
                )
            )
        )
    );

    $res[] = $insertNew = $Sql->insert(
        array(
            'sql' => '
                        INSERT INTO ' . $Sql->tblperson . '
                        ' . $Sql->insertFields(
                array(
                    'insertFields' => array(
                        'fname' => '' . $firstname . '',
                        'lname' => '' . $lastname . '',
                        'nic' => '' . $nic . '',
                        'dob' => '' . $dob . '',
                        'gender' => '' . $gender . '',
                        'mobile' => '' . $mobile . '',
                        'email' => '' . $email . '',
                        'password' => '' . md5($password) . '',
                        'address' => $address,
                        'user_type' => $userType,
                        'user_id' => $userId,
                    )
                )
            )
        )
    );

    
}
else if ($userType == "Guest") {
    $res[] = $insertNew = $Sql->insert(
        array(
            'sql' => '
                        INSERT INTO ' . $Sql->tbllogin . '
                        ' . $Sql->insertFields(
                array(
                    'insertFields' => array(
                        'user_id' => '' . $userId . '',
                        'email' => '' . $email . '',
                        'password' => '' . md5($password) . '',
                        'user_type' => '' . $userType . '',

                    )
                )
            )
        )
    );

    $res[] = $insertNew = $Sql->insert(
        array(
            'sql' => '
                        INSERT INTO ' . $Sql->tblperson . '
                        ' . $Sql->insertFields(
                array(
                    'insertFields' => array(
                        'fname' => '' . $firstname . '',
                        'lname' => '' . $lastname . '',
                        'nic' => '' . $nic . '',
                        'dob' => '' . $dob . '',
                        'gender' => '' . $gender . '',
                        'mobile' => '' . $mobile . '',
                        'email' => '' . $email . '',
                        'password' => '' . md5($password) . '',
                        'address' => $address,
                        'user_type' => $userType,
                        'user_id' => $userId,
                    )
                )
            )
        )
    );

    $res[] = $insertNew = $Sql->insert(
        array(
            'sql' => '
                        INSERT INTO ' . $Sql->tblguest . '
                        ' . $Sql->insertFields(
                array(
                    'insertFields' => array(
                        'guest_id' => '' . $userId . '',
                        'adult' => '' . $adult . '',
                        'children' => '' . $children . '',
                        
                    )
                )
            )
        )
    );

}
echo json_encode($res);
endif;
?>