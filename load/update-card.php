<?php
if ($_SERVER['REQUEST_METHOD'] === "POST") :
    include '../admin/inc/autoload.php';

$User = new User;
$Sql = new Sql;

//$User->validUserForLogin();
//$activeUser = $User->activeUser();
//$activeUser->ID;
$requiredFields = array_fill_keys(
    array(
        'cardtype',
        'cardname',
        'expire',
        'cardnum',
        'cvv',
        'auto'
    ),
    null
);

extract(
    array_intersect_key(
        array_merge(
            $requiredFields,
            $_POST
        ),
        $requiredFields
    )
);

header('Content-type: application/json');

$updates = array(
    'updateFields' => array(
       
        'card_type' => '' . $cardtype . '',
        'name' => '' . $cardname . '',
        'number' => '' . $cardnum . '',
        'expire' => '' . $expire . '',
        'cvv' => '' . md5($cvv) . '',
        

    )
);
$res = $updateUser = $Sql->update(
    array(
        'sql' => '
							UPDATE ' . $Sql->tblcard . '
							' . $Sql->updateFields($updates) . '
							WHERE 1								 
                                AND auto_id="' . $auto . '"
                               
							'
    )
);



echo json_encode($res);
//$res['test']= $rent;
endif;
?>
