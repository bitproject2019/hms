<?php
if ($_SERVER['REQUEST_METHOD'] === "POST") :
    include '../admin/inc/autoload.php';

$User = new User;
$Sql = new Sql;

//$User->validUserForLogin();
//$activeUser = $User->activeUser();
//$activeUser->ID;
$requiredFields = array_fill_keys(
    array(

        'branch',
        'roomType',
        'roomNo',
        'status',
        'auto'

    ),
    null
);

extract(
    array_intersect_key(
        array_merge(
            $requiredFields,
            $_POST
        ),
        $requiredFields
    )
);


    $updates = array(
        'updateFields' => array(
            'branch_no' => '' . $branch . '',
            'room_type' => '' . $roomType . '',
            'room_number' => '' . $roomNo . '',
            'room_status' => '' . $status . '',

        )
    );
    $updateUser = $Sql->update(
        array(
            'sql' => '
							UPDATE ' . $Sql->tblstatus . '
							' . $Sql->updateFields($updates) . '
							WHERE 1								 
                                AND auto_id = "' . $auto . '"
                                
							'
        )
    );




//echo json_encode($res);
endif;
?>