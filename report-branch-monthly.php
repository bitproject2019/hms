<?php
include_once('header.php');
include_once('left-sidebar.php');
include 'admin/inc/autoload.php';
?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />
    <title>Travelers Hotel</title>
    <script>
    var date; 
    </script>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
	<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!--bootstrap -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="assets/plugins/material/material.min.css">
	<link rel="stylesheet" href="assets/css/material_style.css">
	<!-- data tables -->
    <link href="assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
	<!-- animation -->
	<link href="assets/css/pages/animate_page.css" rel="stylesheet">
	<!-- Template Styles -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
	<!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" /> 
	
	<!-- dropzone -->
    <link href="assets/plugins/dropzone/dropzone.css" rel="stylesheet" media="screen">
    
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">

    <div class="page-wrapper">
     
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    <span></span>
                </a>
             
            <div class="page-content-wrapper">
            
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Monthly Sales</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Monthly Sales</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Monthly Sales</li>
                            </ol>
                        </div>
                    </div>
                    
							          
                     <div class="row">
                        <div class="col-md-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header>Monthly Sales</header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <div class="row p-b-20">
                                  <div class="col-lg-3 p-t-5"> 
                                  
						                      
                                              <select class="form-control  select2" id="month" name="month">
                                              <option value="">Select Month</option>
                                              <option value="01">January</option>
                                              <option value="02">February</option>
                                              <option value="03">March</option>
                                              <option value="04">April</option>
                                              <option value="05">May</option>
                                              <option value="06">June</option>
                                              <option value="07">July</option>
                                              <option value="08">August</option>
                                              <option value="09">September</option>
                                              <option value="10">October</option>
                                              <option value="11">November</option>
                                              <option value="12">December</option>
                                              </select>
                                             
						                        </div>
						                  <div class="col-lg-4 p-t-5"> 
                                          <label class = "" for="year" >Year</label>
                                             <input class = "" type = "text" id = "year" name="year" placeholder="2019" >
							            </div>
                                         <div class="col-lg-2 p-t-5 "> 
							              	<button type="submit" class="mdl-button  btn-pink" id="submit" name="submit">Generate</button>
											
							            </div>
                                      
                                        
                                    </div>
                                    
                                    <div class="table-scrollable">
                                    <table class="table table-hover table-checkable order-column full-width" id="example">
                                        <thead>
                                            <tr>
												<th>Travelers Spring</th>
												<th class="center">Travelers Mountain </th>
												<th class="center"> Travelers Lagoon </th>
												
												
												
												
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
            $conn = new mysqli("localhost", "root", "", "travelers_hotel");
          
            $sql = $conn->query("select sum(amount) as 'total' from travelers_payment WHERE MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE()) and branch='Travelers Spring'");
            while ($data = $sql->fetch_array()) {
                if ($data['total'] != "")
                    $tot = $data['total'] . ' . 00 LKR';
                else
                    $tot = '0 . 00 LKR';
            }
            $sql = $conn->query("select sum(amount) as 'total' from travelers_payment WHERE MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE()) and branch='Travelers Mountain'");
            while ($data = $sql->fetch_array()) {
                if ($data['total'] != "")
                    $totm = $data['total'] . ' . 00 LKR';
                else
                    $totm = '0 . 00 LKR';
            }
            $sql = $conn->query("select sum(amount) as 'total' from travelers_payment WHERE MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE()) and branch='Travelers Lagoon'");
            while ($data = $sql->fetch_array()) {
                if ($data['total'] != "")
                    $totl = $data['total'] . ' . 00 LKR';
                else
                    $totl = '0 . 00 LKR';
            }
            
            
                echo '
                                                        <tr class="odd gradeX">
                                                            <td class="user-circle-img sorting_1">
                                                                ' . $tot . '
                                                            </td>
                                                            <td class="center">' . $totm . '</td>
                                                            <td class="center">' . $totl . '</td>
														
                                                            
                                                            
                                                       
                                                        ';
                                                        //$income = $data['amount'];
            
            ?>
										</tbody>
                                    </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
           
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
	  <?php #endregion
    include_once('footer.php');
    ?>
        <!-- end footer -->
	</div>
	

	
		<script type="text/javascript" class="init">
	
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
       buttons: [
        
            {
                extend: 'collection',
                text: 'Export <i class="fa fa-angle-down"></i>',
                buttons: [
                    {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> Copy',
                titleAttr: 'Copy'
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i> Excel',
                titleAttr: 'Excel',
				
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> CSV',
                titleAttr: 'CSV'
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i> PDF',
                titleAttr: 'PDF'
            },
			{
                extend:    'print',
                text:      '<i class="fa fa-print"></i> Print',
                titleAttr: 'print'
            }
                ]
            }
        ]
    } );
	//$("#example_filter").css("float","left");
	$(".dt-buttons").css("float","left");
	$(".dt-buttons").css("float","left");
  /* date =""
   $.ajax({
			method:'POST',
			url:"<?= URL ?>/load/daily-total-sales.php",
			data:{date:date},
			dataType:"text",
			success: function(data) {
				 //$('#example').html(data);
                  $('#income').val(data);
				 console.log(data);
			}
		});*/
	//$(".dt-buttons").css("background-color","deep-pink");
} );
	</script>
    <script>
    $('#submit').click(function(){
		
		
		month = document.getElementById("month").value;
        year = document.getElementById("year").value;
		
		
			$.ajax({
			method:'POST',
			url:"load/monthly-total-branch-spring.php",
			data:{month:month,year:year},
			dataType:"text",
			success: function(data) {
				 $('#example').html(data);
                  //$('#income').val(data);
				 console.log(data);
                 console.log(month);
                 console.log(year);
			}
		});

        	
		
		
		
		
	})
    </script>
</body>
</html>