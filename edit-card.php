<?php
require_once('header.php');
require_once('left-sidebar.php');
include 'admin/inc/autoload.php';

?>
<html>
<head>
</head>
<body>

<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Credit card</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Credit card</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Credit card</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
						<div class="col-sm-12">
							<div class="card-box">
								<div class="card-head">
									<header>Credit card Details</header>
									<button id = "panel-button" 
			                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
			                           data-upgraded = ",MaterialButton">
			                           <i class = "material-icons">more_vert</i>
			                        </button>
			                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
			                           data-mdl-for = "panel-button">
			                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
			                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
			                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
			                        </ul>
                                </div>
                                  <?php
                                    $conn = new mysqli("localhost", "root", "", "travelers_hotel");
                                    $sql = $conn->query("select * from travelers_card where auto_id='$_GET[rno]'");
                                    $auto = $_GET['rno'];
                                    while ($data = $sql->fetch_array()) {



                                        ?>
								<div class="card-body row">
						             <div class="col-lg-6 p-t-20" id="card-type-div" > 
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="card-type" name="card-type" readonly tabIndex="-1" value="<?php echo ($data['card_type']); ?>">
												<label for="card-type" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="card-type" class="mdl-textfield__label">Card Type</label>
												<ul data-mdl-for="card-type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="1">Visa</li>
													<li class="mdl-menu__item" data-val="2">Master</li>
													
												</ul>
											</div>
										</div>
											<div class="col-lg-6 p-t-20"  id="card-name-div"  >
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        id = "card-name" value="<?php echo ($data['name']); ?>">
					                     <label class = "mdl-textfield__label" for = "card-name">Card Name</label>
					                     
					                  </div>
                                    </div>
									
									
                                            
										<div class="col-lg-6 p-t-20"  id="card-num-div"  >
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" name="creditcard" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "card-num" maxlength="16" minlength="16" value="<?php echo ($data['number']); ?>">
					                     <label class = "mdl-textfield__label" for = "card-num">Card Number</label>
					                     <span class = "mdl-textfield__error">Correct number required!</span>
					                  </div>
                                    </div>
									<div class="col-lg-6 p-t-20"  id="expire-div"   >
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "expire" placeholder="mm/yy" value="<?php echo ($data['expire']); ?>">
					                     <label class = "mdl-textfield__label" for = "expire">Expire Date</label>
					                     
					                  </div>
                                    </div>
									<div class="col-lg-6 p-t-20"  id="cvv-div"  >
						               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
					                     <input class = "mdl-textfield__input" type = "text" 
					                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "cvv" maxlength="3" minlength="3" value="<?php echo ($data['cvv']); ?>">
					                     <label class = "mdl-textfield__label" for = "cvv">CVV</label>
					                     <span class = "mdl-textfield__error">Correct number required!</span>
					                  </div>
                                    </div>
						           
                                    <div class="col-lg-6 p-t-20 text-center" >
						              	<button type="submit" id="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
										<button type="button" id="cancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
									</div>
                                     
									</div>
						              <?php

                }
                ?>   
							        
								</div>
							</div>
						</div>
					</div> 
                </div>
            </div>

            <?php require_once('footer.php'); ?>
<!-- start js include path -->

</body>
<?php
   // $total = $rent * $_GET['days'];
   // print_r('sada :'. $total);
?>
<script>
$(document).ready(function(e) {
	
	$('#submit').click(function(){
		
			
			var auto = "<?php echo $auto; ?>"; 
			var cardtype = document.getElementById("card-type").value;
			var cardname = document.getElementById("card-name").value;
			var expire = document.getElementById("expire").value;
			var cardnum = document.getElementById("card-num").value;
			var cvv = document.getElementById("cvv").value;
			//var address = document.getElementById("address").value;
			
			$.post('load/update-card.php',{auto:auto,cardtype:cardtype,cardname:cardname,expire:expire,cardnum:cardnum,cvv:cvv},function(res) {	
				console.log(res);
			});
			
            
           
			document.getElementById("card-type").value = "";
			document.getElementById("card-name").value = "";
			document.getElementById("expire").value = "";
			document.getElementById("card-num").value = "";
            document.getElementById("cvv").value = "";
            window.alert("updated Successfully!");
            window.location.assign('all-card.php');
			
	})


})
</script>
</html>