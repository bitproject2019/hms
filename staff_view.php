<?php
require_once('header.php');
require_once('left-sidebar.php');
include 'admin/inc/autoload.php';
?>
          <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">All Staffs</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Staffs</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">All Staffs</li>
                            </ol>
                        </div>
                    </div>
					<ul class="nav nav-pills nav-pills-rose">
						<li class="nav-item tab-all"><a class="nav-link active show"
							href="#tab1" data-toggle="tab">List View</a></li>
						<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
							data-toggle="tab">Grid View</a></li>
					</ul>
					<div class="tab-content tab-space">
	                   <div class="tab-pane active show" id="tab1">
						 <div class="row">
		                    <div class="col-md-12">
	                            <div class="card-box">
	                                <div class="card-head">
	                                    <button id = "panel-button" 
				                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
				                           data-upgraded = ",MaterialButton">
				                           <i class = "material-icons">more_vert</i>
				                        </button>
				                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
				                           data-mdl-for = "panel-button">
				                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
				                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
				                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
				                        </ul>
	                                </div>
	                                <div class="card-body ">
	                                  <div class="table-scrollable">
										<table class="table table-hover table-checkable order-column full-width" id="example4">
												<thead>
													<tr>
														<th>Users</th>
														<th class="center"> First Name </th>
														<th class="center"> Last Name </th>
														<th class="center"> NIC No </th>
														<th class="center"> Mobile No </th>
														<th class="center"> Email </th>
														
														<th class="center"> Action </th>
													</tr>
												</thead>
												<tbody>
												<?php
														$conn = new mysqli("localhost","root","","travelers_hotel");
														$sql =  $conn->query("select * from travelers_person");
														while($data = $sql->fetch_array()){
															echo'
																<tr class="odd gradeX">
																	<td class="user-circle-img sorting_1">
																		'.$data['user_type'].'
																	</td>
																	<td class="center">'.$data['fname'].'</td>
																	<td class="center">'.$data['lname'].'</td>
																	<td class="center">'.$data['nic'].'</td>
																	<td class="center">
																		<a href="tel:444786876">
																			'.$data['mobile'].' 
																		</a>
																	</td>
																	<td class="center">
																		<a href="mailto:shuxer@gmail.com">
																			'.$data['email'].' 
																		</a>
																	</td>
																	
																	
																	<td class="center">
																		<a href="edit_staff.html" class="btn btn-tbl-edit btn-xs">
																			<i class="fa fa-pencil"></i>
																		</a>
																		<a class="btn btn-tbl-delete btn-xs">
																			<i class="fa fa-trash-o "></i>
																		</a>
																	</td>
																</tr>
																';
														}
																?>
												</tbody>
											</table>
	                                </div>
	                            </div>
		                      </div>
		                  </div>
	                    </div>
						
    <?php include_once('footer.php');                                            