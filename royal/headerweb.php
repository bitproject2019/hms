<?php
	session_start();
?>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="image/favicon.png" type="image/png">
        <title>Travelers Hotel</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="vendors/linericon/style.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="vendors/bootstrap-datepicker/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
        <!-- main css -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    <body>
        <!--================Header Area =================-->
        <header class="header_area">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <a class="navbar-brand logo_h" href="index.html"><img src="image/Logo.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                        <ul class="nav navbar-nav menu_nav ml-auto">
                            <li class="nav-item active"><a class="nav-link" href="index.php">Home</a></li> 
                            
                            <li class="nav-item"><a class="nav-link" href="accomodation.php">Accomodation</a></li>
                            <li class="nav-item"><a class="nav-link" href="gallery.php">Gallery</a></li>
                            <li class="nav-item submenu dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Branches</a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a class="nav-link" href="blog-single.php">Travelers Mountain</a></li>
                                    <li class="nav-item"><a class="nav-link" href="blog.php">Travelers Spring</a></li>
                                    <li class="nav-item"><a class="nav-link" href="blog-single.php">Travelers Lagoon</a></li>

                                </ul>
                            </li> 
                            <li class="nav-item"><a class="nav-link" href="contact.php">Contact Us</a></li>
                            <li class="nav-item"><a class="nav-link" href="about.php">About us</a></li>
                            <li class="nav-item"><a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal">Login</a></li>
							<li class="nav-item"><a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal3">Sign Up</a></li>
                        </ul>
                    </div> 
                </nav>
            </div>
        </header>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Login</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
				</div>
				<?php 
			//if (isset($message)) {
				
				?><!--
					<div class="form-group">
			<div class="alert alert-warning" id="loginAlert" >
				<?php 
	// echo '<label class="text-danger">'.$message.'</label>';
	// ?>
	</div>
	</div>-->
	<?php
			//}
			?>  
			  <div class="modal-body">
				<form id="loginForm" method="post" action="login.php">
				  <div class="form-group">
					<label for="email" class="col-form-label">Username:</label>
					<input type="text" class="form-control" id="email" name="uname">
				  </div>
				  <div class="form-group">
					<label for="pwd" class="col-form-label">Password:</label>
					<input type="password" class="form-control" id="pwd" name="pass">
					</div>
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="loginAdmin" id="loginAdmin">Login</button>
</div>
  	
  
					</form>
			  </div>
			</div>
		  </div>
		</div>
		<!--================Modal login Area END=================-->
		<!--================Sign Up form Modal =================-->
			<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Sign up</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				  </div>
				  <div class="modal-body">
					<form>
					  <div class="form-row">
						<div class="form-group col-md-6">
						  <label for="inputEmail4">Firstname</label>
						  <input type="text" class="form-control" id="fname" placeholder="Firstname">
						</div>
						<div class="form-group col-md-6">
						  <label for="inputPassword4">Surname</label>
						  <input type="text" class="form-control" id="lname" placeholder="Surname">
						</div>
						<div class="form-group col-md-6">
						  <label for="inputEmail4">Email</label>
						  <input type="email" class="form-control" id="user" placeholder="Email">
						</div>
						<div class="form-group col-md-6">
						  <label for="inputPassword4">Contact No</label>
						  <input type="number" class="form-control" id="phone" placeholder="Contact No">
						</div>
						<div class="form-group col-md-6">
						  <label for="inputPassword4">Date of Reg</label>
						  <input type="text" class="form-control" id="date" placeholder="Date of Reg">
						</div>
						<div class="form-group col-md-6">
						  <label for="inputPassword4">Password</label>
						  <input type="password" class="form-control" id="password" placeholder="Password">
						</div>
					  </div>
					  <div class="form-group">
						<label for="inputAddress">Address</label>
						<input type="text" class="form-control" id="address" placeholder="Address">
					  </div>
					</form>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">Save changes</button>
				  </div>
				</div>
			  </div>
			</div>
</body>

 </html>
