<?php
require_once('header.php');
require_once('left-sidebar.php');
include 'admin/inc/autoload.php';
?>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/dob.js" type="text/javascript"> </script>
</head>
<body>
<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Edit Staff Details</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Staff</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Edit Staff Details</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
							<div class="col-sm-12">
								<div class="card-box">
									<div class="card-head">
										<header>Basic Information</header>
										<button id = "panel-button" 
				                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
				                           data-upgraded = ",MaterialButton">
				                           <i class = "material-icons">more_vert</i>
				                        </button>
				                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
				                           data-mdl-for = "panel-button">
				                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
				                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
				                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
				                        </ul>
                                    </div>
                                    					 <?php
                                            $conn = new mysqli("localhost", "root", "", "travelers_hotel");
                                            $sql = $conn->query("select * from travelers_person where user_id='$_GET[rno]'");
                                            $auto = $_GET['rno'];
                                            while ($data = $sql->fetch_array()) {
                                               

                                              

                                                ?>
									<div class="card-body row">
									<div class="col-lg-6 p-t-20"> 
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
											<input class="mdl-textfield__input" type="text" id="user-type"  name="user-type" readonly tabIndex="-1" value="<?php echo ($data['user_type']); ?>">
											<label for="user-type" class="pull-right margin-0">
												<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
											</label>
											<label for="user-type" class="mdl-textfield__label">User Type</label>
											<ul data-mdl-for="user-type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
												<li class="mdl-menu__item" data-val="BY">Guest</li>
												<li class="mdl-menu__item" data-val="BY">Admin</li>
												<li class="mdl-menu__item" data-val="DE">Director</li>
												<li class="mdl-menu__item" data-val="BY">Sales</li>
												<li class="mdl-menu__item" data-val="DE">Manager</li>
												<li class="mdl-menu__item" data-val="BY">IT Manager</li>
												<li class="mdl-menu__item" data-val="BY">Worker</li>
											</ul>
										</div>
							            </div>
							            <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "text" id = "fname" value="<?php echo ($data['fname']); ?>">
						                     <label class = "mdl-textfield__label" >First Name</label>
						                  </div>
							            </div>
							             <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "email" id = "lname" value="<?php echo ($data['lname']); ?>">
						                     <label class = "mdl-textfield__label" >Surname</label>
						                   </div>
							            </div>
										<div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "text" id="nic" maxlength="12" minlength="10" value="<?php echo ($data['nic']); ?>">
						                     <label class = "mdl-textfield__label" >NIC No</label>
											  <span class = "mdl-textfield__error">Enter Valid NIC No</span>
											  
						                  </div>
							            </div>
										<div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "text" id="dob" placeholder="" readonly value="<?php echo ($data['dob']); ?>">
						                     <label class = "mdl-textfield__label" >DOB</label>
						                     
						                  </div>
										</div>
										<div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "text" id="gender" placeholder="" readonly value="<?php echo ($data['gender']); ?>">
						                     <label class = "mdl-textfield__label" >Gender</label>
						                     
						                  </div>
							            </div>
										<div class="col-lg-6 p-t-20">
							               <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "text" 
						                        pattern = "-?[0-9]*(\.[0-9]+)?" id = "mobile" maxlength="10" value="<?php echo ($data['mobile']); ?>">
						                     <label class = "mdl-textfield__label" for = "mobile">Mobile Number</label>
						                     <span class = "mdl-textfield__error">Number required!</span>
						                  </div>
							            </div>
										<div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "email" id = "email" value="<?php echo ($data['email']); ?>">
						                     <label class = "mdl-textfield__label" >Email</label>
						                      <span class = "mdl-textfield__error">Enter Valid Email Address!</span>
						                  </div>
							            </div>
										<?php  $password=($data['password']); ?>
										
	                                 	<!--<div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
											 <input class = "mdl-textfield__input" type = "password" id = "confirm_password" minlength="8" value="<?php //echo ($data['password']); ?>" >
											 
											 <input  type = "text" id = "pass" style="color: #d50000; position: absolute; font-size: 12px; margin-top: 3px; border: none; display: block;" >
											 <label class = "mdl-textfield__label" >Confirm Password</label>
											 <span class = "mdl-textfield__error" id=""></span>
						                  </div>
							            </div>
										<div class="col-lg-6 p-t-20"> 
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
												<input class="mdl-textfield__input" type="text" id="gender" value="" readonly tabIndex="-1">
												<label for="gender" class="pull-right margin-0">
													<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
												</label>
												<label for="gender" class="mdl-textfield__label">Gender</label>
												<ul data-mdl-for="gender" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
													<li class="mdl-menu__item" data-val="1">Male</li>
													<li class="mdl-menu__item" data-val="2">Female</li>
												</ul>
											</div>
										</div>-->
							            <div class="col-lg-12 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <textarea class = "mdl-textfield__input" rows =  "1" 
						                        id = "address" ><?php echo ($data['address']); ?></textarea>
						                     <label class = "mdl-textfield__label" for = "address">Address</label>
						                  </div>
                                         </div>

                                         <?php

                                   
                                if ($data['user_type'] == "Guest") {
                                    $sql = $conn->query("select * from travelers_guest where guest_id='$_GET[rno]'");
                                    $auto = $_GET['rno'];
                                    while ($data = $sql->fetch_array()) {
                                                        
                                         ?>
							            <div class="col-lg-6 p-t-20"> 
											<div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" id="display-adult" hidden >
												<input class = "mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="adult" name = "adult" value="<?php echo ($data['adult']); ?>">
												<label class = "mdl-textfield__label" for="adult">No Of Adult</label>
												<span class = "mdl-textfield__error">Number required!</span>
											</div>
							            </div>
										<div class="col-lg-6 p-t-20"> 
											<div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" id="display-child" hidden>
												<input class = "mdl-textfield__input" type="text" pattern = "-?[0-9]*(\.[0-9]+)?" id="children" name = "children" value="<?php echo ($data['children']); ?>">
												<label class = "mdl-textfield__label" for = "children">No Of Children</label>
												<span class = "mdl-textfield__error">Number required!</span>
											</div>
							            </div>
                                        						              <?php


														}
													}
														else {
															?>
															<div class="col-lg-6 p-t-20"> 
											<div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" id="display-adult" hidden >
												<input class = "mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="adult" name = "adult" value="">
												<label class = "mdl-textfield__label" for="adult">No Of Adult</label>
												<span class = "mdl-textfield__error">Number required!</span>
											</div>
							            </div>
										<div class="col-lg-6 p-t-20"> 
											<div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" id="display-child" hidden>
												<input class = "mdl-textfield__input" type="text" pattern = "-?[0-9]*(\.[0-9]+)?" id="children" name = "children" value="">
												<label class = "mdl-textfield__label" for = "children">No Of Children</label>
												<span class = "mdl-textfield__error">Number required!</span>
											</div>
							            </div>
										<?php
														}
                                                        }
                                                        
                                                    
                                                        ?> 
														<div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" type = "password" id = "password"  value="">
											 <label class = "mdl-textfield__label" >Enter the password to confirm</label>
											 
						                  </div>
							            </div>  
								         <div class="col-lg-12 p-t-20 text-center"> 
							              	<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" id="submit">Submit</button>
											<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" id="cancel">Cancel</button>
							            </div>
									</div>
								</div>
							</div>
						</div> 
                </div>
            </div>
<?php require_once('footer.php'); ?>
<!-- start js include path -->




</body>
<script src="js/jquery.md5.js"></script>
<script>

$(document).ready(function(e) {
	

		if($('#user-type').val()=="Guest")
		{
 			$('#display-child').removeAttr('hidden');
	 	    $('#display-adult').removeAttr('hidden');
		}
		else
		{
			$('#user-type').change(function(){
				console.log($('#user-type').val());
				if($('#user-type').val()=="Guest")
				{
					$('#display-child').removeAttr('hidden');
					$('#display-adult').removeAttr('hidden');
				}
				else
				{
					$('#display-child').attr("hidden","true");
					$('#display-adult').attr("hidden","true");
				}
			
			})
			//$('#display-child').attr("hidden","true");
	  		//$('#display-adult').attr("hidden","true");
		}
	 
			var password = '<?php echo $password; ?>';
			console.log('pass'+password);

	$('#submit').click(function(){
		    console.log('pass'+password);
			var password = '<?php echo $password; ?>';
			console.log(password);
			var enteredpassword = $('#password').val();
			var md5 = $.md5($('#password').val());
			console.log('md5 '+md5);
		 if (md5 == password) {
			
            var auto = "<?php echo $auto;?>";
			var userType = document.getElementById("user-type").value;
			var firstname = document.getElementById("fname").value;
			var lastname = document.getElementById("lname").value;
			var nic = document.getElementById("nic").value;
			var dob = document.getElementById("dob").value;
			var gender = document.getElementById("gender").value;
			var mobile = document.getElementById("mobile").value;
			var email = $('#email').val();
			//var password = document.getElementById("password").value;
			var address = document.getElementById("address").value;
            if(userType!="Guest")
            {
                var children = "";
                var adult =  "";
            }
            else
            {
                var children = document.getElementById("children").value;
			    var adult = document.getElementById("adult").value;
            }
			
			$.post('load/update-persons.php',{auto:auto,userType:userType,firstname:firstname,lastname:lastname,nic:nic,dob:dob,gender:gender,mobile:mobile,email:email,address:address,children:children,adult:adult},function(res) {
								
					console.log(res);
					window.alert("updated Successfully!");
					window.location.assign('all-persons.php');
			});
			document.getElementById('pass').value = "";
            
		 }
		 else{
			window.alert("Recheck Password");
		 }
			
	})


})
</script>
</html>
<!-- end js include path -->