<?php
include('connection.php');
$conn = new mysqli("localhost", "root", "", "travelers_hotel");
/*$sql = $conn->query("SELECT auto_id FROM travelers_room ORDER BY auto_id DESC LIMIT 1");
while ($data = $sql->fetch_array()) {
    $id = $data['auto_id'];
}
if (count($_FILES["image"]["tmp_name"]) > 0) {
    for ($count = 0; $count < count($_FILES["image"]["tmp_name"]); $count++) {
        $image_file = addslashes(file_get_contents($_FILES["image"]["tmp_name"][$count]));
        $query = "INSERT INTO travelers_images(auto_id,images) VALUES ('$id','$image_file')";
        $statement = $connect->prepare($query);
        $statement->execute();
    }
}*/

$id = $auto;
if(isset($_POST['submit'])){
    // Include the database configuration file
  //  include_once 'dbConfig.php';
    
    // File upload configuration
    $targetDir = "uploads/";
    $allowTypes = array('jpg','png','jpeg','gif');
    
    $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = '';
    if(!empty(array_filter($_FILES['files']['name']))){
        foreach($_FILES['files']['name'] as $key=>$val){
            // File upload path
            $fileName = basename($_FILES['files']['name'][$key]);
            $targetFilePath = $targetDir . $fileName;
            
            // Check whether file type is valid
            $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
            if(in_array($fileType, $allowTypes)){
                // Upload file to server
                if(move_uploaded_file($_FILES["files"]["tmp_name"][$key], $targetFilePath)){
                    // Image db insert sql
                    $insertValuesSQL .= "('".$fileName."', NOW(),'$id'),";
                }else{
                    $errorUpload .= $_FILES['files']['name'][$key].', ';
                }
            }else{
                $errorUploadType .= $_FILES['files']['name'][$key].', ';
            }
        }
        
        if(!empty($insertValuesSQL)){
            $insertValuesSQL = trim($insertValuesSQL,',');
            // Insert image file name into database
            $insert = $conn->query("INSERT INTO travelers_new_images (file_name, uploaded_on,id) VALUES $insertValuesSQL");
            if($insert){
                $errorUpload = !empty($errorUpload)?'Upload Error: '.$errorUpload:'';
                $errorUploadType = !empty($errorUploadType)?'File Type Error: '.$errorUploadType:'';
                $errorMsg = !empty($errorUpload)?'<br/>'.$errorUpload.'<br/>'.$errorUploadType:'<br/>'.$errorUploadType;
                $statusMsg = "Files are uploaded successfully.".$errorMsg;
                header('location:all-room.php');
            }else{
                 header('location:all-room.php');
               
            }
        }
    }else{
        
         header('location:all-room.php');
         
         
        
    }
    
    // Display status message
    echo $statusMsg;
}
?>
